﻿using System;
using System.Security;
using System.Security.Cryptography;

namespace Valut.Accounts
{
    public interface IContaMain
    {
        DateTime CriadoEm { get; set; }
        string Dominio { get; set; }
        bool Favorito { get; set; }
        DateTime ModificadoEm { get; set; }
        string Obs { get; set; }
        SecureString Senha { get; set; }
        string Usuario { get; set; }
        SerializableConta GetSerializableAccount();
        SerializableConta GetSerializableAccount(RSAParameters keyinfo);
    }
}