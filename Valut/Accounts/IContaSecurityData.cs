﻿namespace Valut.Accounts
{
    public interface IContaSecurityData
    {
        byte[] Assinatura { get; set; }
        byte[] Hash { get; set; }
        bool Integridade { get; set; }
    }
}