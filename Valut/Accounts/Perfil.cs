﻿using System;

namespace Valut.Accounts
{
    [Serializable]
    public class Perfil
    {
        public string Nome { get; internal set; }
        public DateTime Criação { get; }
        public DateTime Modificação { get; internal set; }
        public int PasswordExpirationTime { get; internal set; }

        public Perfil(string nome, DateTime criação)
        {
            Nome = nome;
            Modificação = Criação = criação;
            PasswordExpirationTime = 5;
        }

        public Perfil(string nome, DateTime criação, int passwordExpiration)
        {
            Nome = nome;
            Modificação = Criação = criação;
            PasswordExpirationTime = passwordExpiration;
        }
    }
}
