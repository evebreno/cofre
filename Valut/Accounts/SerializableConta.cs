﻿using System;
using System.Text;
using System.Security.Cryptography;
using Valut.Encrypting;

namespace Valut.Accounts
{
    [Serializable]
    public class SerializableConta
    {
        private string _dominio;
        private string _usuario;
        private bool _favorito;
        private string _obs;
        private DateTime _criadoEm;
        private DateTime _modificadoEm;
        private byte[] _senha;
        private bool _integridade;
        private byte[] _hash;
        private byte[] _assinatura;

        public SerializableConta(string dominio, string usuario, bool favorito, byte[] senha, string obs, DateTime criadoEm, DateTime modificadoEm, byte[] hash, byte[] assinatura)
        {
            _dominio = dominio;
            _usuario = usuario;
            _favorito = favorito;
            _obs = obs;
            _criadoEm = criadoEm;
            _modificadoEm = modificadoEm;
            _senha = senha;
            _hash = hash;
            _assinatura = assinatura;
        }

        /// <summary>
        /// Obtém a conta no modo não serializável com a senha criptografada
        /// </summary>
        /// <param name="key">Chave para criptografar a senha</param>
        public Conta GetNonSerializableAccount(RSAParameters key)
        {
            var data = RSACrypter.DecryptData(_senha, key, true);
            string senha = Encoding.Unicode.GetString(data);
            var conta = new Conta(_dominio, _usuario, _favorito, senha, _obs, _criadoEm, _modificadoEm, _hash, _assinatura);
            conta.EncryptPassword(key);

            return conta;
        }
    }
}
