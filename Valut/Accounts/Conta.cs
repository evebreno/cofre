﻿using System;
using System.Text;
using System.Security;
using System.Security.Cryptography;
using Valut.Encrypting;

namespace Valut.Accounts
{
    [Serializable]
    public class Conta : IContaMain, IContaSecurityData
    {
        private string _dominio;
        private string _usuario;
        private bool _favorito;
        private string _obs;
        private DateTime _criadoEm;
        private DateTime _modificadoEm;
        private SecureString _senha;
        [NonSerialized] private bool _encrypted = false;
        [NonSerialized] private bool _integridade;
        [NonSerialized] private byte[] _hash;
        [NonSerialized] private byte[] _assinatura;

        #region Encapsulamento
        public string Dominio { get => _dominio; set => _dominio = value; }
        public string Usuario { get => _usuario; set => _usuario = value; }
        public bool Favorito { get => _favorito; set => _favorito = value; }
        public string Obs { get => _obs; set => _obs = value; }
        public DateTime CriadoEm { get => _criadoEm; set => _criadoEm = value; }
        public DateTime ModificadoEm { get => _modificadoEm; set => _modificadoEm = value; }
        public SecureString Senha { get => _senha; set => _senha = value; }
        public bool Integridade { get => _integridade; set => _integridade = value; }
        /// <summary>
        /// Verifica se a senha está íntegra
        /// </summary>
        public byte[] Hash { get => _hash; set => _hash = value; }
        /// <summary>
        /// Verifica a veracidade de todas as informações
        /// </summary>
        public byte[] Assinatura { get => _assinatura; set => _assinatura = value; }
        #endregion 

        #region Construtores
        public Conta(string dominio, string usuario, bool favorito, string senha, string obs, DateTime criadoEm, DateTime modificadoEm, byte[] hash, byte[] assinatura)
        {
            _dominio = dominio;
            _usuario = usuario;
            _favorito = favorito;
            _obs = obs;
            _criadoEm = criadoEm;
            _modificadoEm = modificadoEm;
            _senha = senha.HideString();
            _hash = hash;
            _assinatura = assinatura;
        }
        public Conta(string dominio, string usuario, string senha, string obs)
        {
            _dominio = dominio;
            _usuario = usuario;
            _favorito = false;
            _obs = obs;
            _criadoEm = DateTime.Now;
            _modificadoEm = DateTime.Now;
            _senha = senha.HideString();
            _hash = new byte[0];
            _assinatura = new byte[0];
        }
        public Conta(string dominio, string usuario, string senha)
        {
            _dominio = dominio;
            _usuario = usuario;
            _favorito = false;
            _obs = "";
            _criadoEm = DateTime.Now;
            _modificadoEm = DateTime.Now;
            _senha = senha.HideString();
            _hash = new byte[0];
            _assinatura = new byte[0];
        }
        #endregion

        public SerializableConta GetSerializableAccount()
        {
            byte[] senha = Encoding.Unicode.GetBytes(_senha.ShowString());
            return new SerializableConta(_dominio, _usuario, _favorito, senha, _obs, _criadoEm, _modificadoEm, _hash, _assinatura);
        }

        public SerializableConta GetSerializableAccount(RSAParameters keyinfo)
        {
            byte[] data = Encoding.Unicode.GetBytes(_senha.ShowString());
            byte[] transform = RSACrypter.EncryptData(data, keyinfo, true);

            return new SerializableConta(_dominio, _usuario, _favorito, transform, _obs, _criadoEm, _modificadoEm, _hash, _assinatura);
        }

        public void EncryptPassword(RSAParameters keyinfo)
        {
            if (_encrypted)
            {
                byte[] data = Encoding.Unicode.GetBytes(_senha.ShowString());
                byte[] transform = RSACrypter.EncryptData(data, keyinfo, true);
                string text = Encoding.Unicode.GetString(transform);
                _senha = text.HideString();
                _encrypted = true;
            }
        }

        public bool DecryptPassword(RSAParameters keyinfo)
        {
            try
            {
                if (!_encrypted)
                {
                    byte[] data = Encoding.Unicode.GetBytes(_senha.ShowString());
                    byte[] transform = RSACrypter.DecryptData(data, keyinfo, true);
                    string text = Encoding.Unicode.GetString(transform);
                    _senha = text.HideString();
                    _encrypted = false;       
                }
                return true;
            }

            catch
            { return false; }
        }
    }
}
