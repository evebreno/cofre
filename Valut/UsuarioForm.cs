﻿using System;
using System.Windows.Forms;
using Valut.Encrypting;

namespace Valut
{
    public partial class UsuarioForm : Form
    {
        public UsuarioForm()
        {
            InitializeComponent();
        }

        private void UsuarioForm_Load(object sender, EventArgs e)
        {
            FillForm();
        }

        private void FillForm()
        {
            var perfil = UserCenterData.PerfilData;
            UserNameTB.Text = perfil.Nome;
            TimeTkB.Value = perfil.PasswordExpirationTime;
            CreationAccTB.Text = perfil.Criação.ToShortDateString();
            LastModifTB.Text = perfil.Modificação.ToShortDateString();
            MinuteLB.Text = (TimeTkB.Value != 0) ? TimeTkB.Value + " minuto(s)" : "Nunca";
        }

        private void CommitNewPassBt_Click(object sender, EventArgs e)
        {
            UserCenterData.AbortPass();
            try
            {
                UserCenterData.GetPass(true);
                if(string.Equals(NewPassTB.Text, CommitPassTB.Text) && !string.IsNullOrWhiteSpace(NewPassTB.Text))
                {
                    UserCenterData.PerfilData.Modificação = DateTime.Today;
                    UserCenterData.SaveData(NewPassTB.Text.HideString());
                    MessageBox.Show("Senha armazenada com sucesso!\nO programa irá reiniciar");
                    Application.Restart();
                }

                else MessageBox.Show("Erro: as senhas não conferem ou estão em branco. Favor verificar");
            }

            catch(EncryptingExceptions.IncorrrectPasswordException)
            { UserCenterData.AbortPass(); MessageBox.Show("Senha incorreta.");}

            catch(EncryptingExceptions.PasswordNotSetException)
            { UserCenterData.AbortPass(); MessageBox.Show("Digite a senha.");}
        }

        private void SaveChangesBT_Click(object sender, EventArgs e)
        {
            try
            {
                UserCenterData.PerfilData.Modificação = DateTime.Today;
                UserCenterData.PerfilData.Nome = UserNameTB.Text;
                UserCenterData.PerfilData.PasswordExpirationTime = TimeTkB.Value;
                UserCenterData.ChangeCounter(TimeTkB.Value);
                UserCenterData.SaveData(UserCenterData.GetPass(true));
                MessageBox.Show("Dados salvos!");
                this.Close();
            }
            catch (EncryptingExceptions.IncorrrectPasswordException)
            {
                MessageBox.Show("Senha incorreta!");
            }
            catch(EncryptingExceptions.PasswordNotSetException)
            {
                MessageBox.Show("Digite a senha.");
            }

            catch(Exception ex)
            {
                MessageBox.Show($"Erro inesperado: {ex.Message}");
            }
        }

        private void TimeTkB_Scroll(object sender, EventArgs e)
        {
            MinuteLB.Text = (TimeTkB.Value != 0) ? TimeTkB.Value + " minuto(s)": "Nunca";
        }
    }
}
