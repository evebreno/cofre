﻿using System;
using System.Security;
using System.Runtime.InteropServices;

namespace Valut.Encrypting
{
    static public class SecureStringXT
    {
        static public SecureString HideString(this string str)
        {
            SecureString st = new SecureString();
            foreach(char ch in str)
            { st.AppendChar(ch); }

            return st;
        }

        static public string ShowString(this SecureString scstr)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(scstr);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }
}
