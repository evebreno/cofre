﻿using System.Security.Cryptography;

namespace Valut.Encrypting
{
    static public class MD5Signer
    {
        static public byte[] GetSignature(byte[] dataToSign)
        {
            MD5CryptoServiceProvider signer = new MD5CryptoServiceProvider();            
            return signer.ComputeHash(dataToSign);
        }

        static public bool VerifySignature(byte[] dataToVerify, byte[] Signature)
        {
            byte[] sign =  GetSignature(dataToVerify);

            if (sign.Length != Signature.Length) return false;

            for(int i =0;i<sign.Length;i++)
            {
                if (sign[i] != Signature[i]) return false;
            }

            return true;
        }
    }
}
