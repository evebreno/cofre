﻿using System;
using System.Security.Cryptography;

namespace Valut.Encrypting
{
    /// <summary>
    /// Criptografia em chave pública/privada
    /// </summary>
    public class RSACrypter
    {
        static public RSAParameters GenerateKey()
        {
            RSACryptoServiceProvider rsacsp = new RSACryptoServiceProvider();
            return rsacsp.ExportParameters(true);
        }

        static public byte[] EncryptData(byte[] data, RSAParameters keyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                using (RSACryptoServiceProvider RSACSP = new RSACryptoServiceProvider())
                {
                    RSACSP.ImportParameters(keyInfo);
                    encryptedData = RSACSP.Encrypt(data, DoOAEPPadding);
                }

                return encryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        static public byte[] DecryptData(byte[] data, RSAParameters keyInfo, bool doOAEPPadding)
        {
            try
            {
                using (RSACryptoServiceProvider RSACPS = new RSACryptoServiceProvider())
                {
                    RSACPS.ImportParameters(keyInfo);
                    byte[] decryptedData = RSACPS.Decrypt(data, doOAEPPadding);
                    return decryptedData;
                }
            }

            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }

        }
    }
}
