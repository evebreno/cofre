﻿using System;

namespace Valut.Encrypting
{
    public class EncryptingExceptions : Exception
    {
        public new string Message { get; protected set; }

        public class IncorrrectPasswordException : EncryptingExceptions
        {
            public IncorrrectPasswordException(string message)
            {
                this.HResult = 1500;
                this.Message = message;
            }
        }

        public class PasswordNotSetException : EncryptingExceptions
        {
            public PasswordNotSetException(string message)
            {
                this.HResult = 1600;
                this.Message = message;
            }
        }
    }
}
