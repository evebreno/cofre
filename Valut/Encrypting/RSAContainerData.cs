﻿using System;
using System.Security.Cryptography;

namespace Valut.Encrypting
{
    [Serializable]
    public class RSAContainerData
    {
        private byte[] D, DP, DQ, Exponent, InverseQ, Modulus, P, Q;
        public RSAContainerData(RSAParameters keyInfo)
        {
            RSACryptoServiceProvider r = new RSACryptoServiceProvider();
            r.ImportParameters(keyInfo);
            if (r.PublicOnly) throw new CryptographicException("É necessário chave privada!");

            D = keyInfo.D;
            DP = keyInfo.DP;
            DQ = keyInfo.DQ;
            Exponent = keyInfo.Exponent;
            InverseQ = keyInfo.InverseQ;
            Modulus = keyInfo.Modulus;
            P = keyInfo.P;
            Q = keyInfo.Q;
        }

        public RSAParameters PrivateKey()
        {
            RSAParameters p = new RSAParameters();
            p.D = D;
            p.DP = DP;
            p.DQ = DQ;
            p.Exponent = Exponent;
            p.InverseQ = InverseQ;
            p.Modulus = Modulus;
            p.P = P;
            p.Q = Q;

            return p;
        }
    }
}
