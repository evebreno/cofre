﻿using System.Security.Cryptography;

namespace Valut.Encrypting
{
    static public class SHA512Signer
    {
        static public byte[] GetSignature(byte[] dataToSign)
        {
            SHA512Managed signer = new SHA512Managed();
            return signer.ComputeHash(dataToSign);
        }

        static public int GetSignatureIdentificator(byte[] dataToSign)
        {
            SHA512Managed signer = new SHA512Managed();
            signer.ComputeHash(dataToSign);
            int v = new int();
            foreach(byte b in signer.Hash)
            {
                v += b;
            }

            return v;
        }

        static public bool VerifySignature(byte[] dataToVerify, byte[] signature)
        {
            byte[] block = GetSignature(dataToVerify);
            if (block.Length == signature.Length)
            {
                for (int i = 0; i < block.Length; i++)
                {
                    if (block[i] != signature[i]) return false;
                }

                return true;
            }
            else return false;
        }
    }
}
