﻿using System;
using System.Security.Cryptography;

namespace Valut.Encrypting
{
    /// <summary>
    /// Criptografia simétrica
    /// </summary>
    public class AESCrypter
    {
        static public byte[] EncryptData(byte[] data, byte[] key, byte[] iv)
        {
            byte[] encryptedData;
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                aes.IV = iv;
                byte[] criptKey = new byte[32];
                Random r = new Random(SHA512Signer.GetSignatureIdentificator(key));
                r.NextBytes(criptKey);
                aes.Key = criptKey;

                var encryptor = aes.CreateEncryptor();
                encryptedData = encryptor.TransformFinalBlock(data, 0, data.Length);
            }

            return encryptedData;
        }

        static public byte[] DecryptData(byte[] data, byte[] key, byte[] iv)
        {
            byte[] decryptedData;
            using (AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
            {
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                aes.IV = iv;
                byte[] criptKey = new byte[32];
                Random r = new Random(SHA512Signer.GetSignatureIdentificator(key));
                r.NextBytes(criptKey);
                aes.Key = criptKey;

                var decryptor = aes.CreateDecryptor();
                decryptedData = decryptor.TransformFinalBlock(data, 0, data.Length);
            }

            return decryptedData;
        }
    }
}
