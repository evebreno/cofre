﻿using System.Collections.Generic;
using System.Security.Cryptography;

namespace Valut.Encrypting
{
    static public class RSAParametersXT
    {
        static public byte[] Key(this RSAParameters par)
        {
            List<byte> buffer = new List<byte>();
            buffer.AddRange(par.Exponent);
            buffer.AddRange(par.Modulus);

            return SHA512Signer.GetSignature(buffer.ToArray());
        }
    }
}
