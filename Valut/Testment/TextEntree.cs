﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Valut.Testment
{
    public partial class TextEntree : Form
    {
        public TextEntree()
        {
            InitializeComponent();
        }

        public TextEntree(string texto, string titulo)
        {
            InitializeComponent();
            this.Text = titulo;
            TextEntrance.Text = texto;
        }

        private void Commit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
