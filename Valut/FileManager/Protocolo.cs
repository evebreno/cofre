﻿using System;

namespace Valut.FileManager
{
    [Serializable]
    public class Protocolo
    {
        public int Major { get => major; set => major = value; }
        public int Minor { get => minor; set => minor = value; }
        public int Revisão { get => revisao; set => revisao = value; }
        public DateTime Data { get => data; set => data = value; }

        private int major;
        private int minor;
        private int revisao;
        private DateTime data;

        public Protocolo()
        {
            Major = 0;
            Minor = 0;
            Revisão = 0;
            Data = new DateTime();
        }

        public Protocolo(int maior, int menor, int revisao)
        {
            Major = maior;
            Minor = menor;
            Revisão = revisao;
            Data = new DateTime();
        }

        public Protocolo(int maior, int menor, int revisao, DateTime data)
        {
            Major = maior;
            Minor = menor;
            Revisão = revisao;
            Data = data;
        }
    }
}
