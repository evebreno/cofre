﻿using System.Collections.Generic;

namespace Valut.FileManager
{
    public class MakeFile
    {
        public byte[] Output { get => actualStream.ToArray(); private set => _output = value; }

        private List<byte> actualStream;
        private byte[] _output;

        public MakeFile()
        {
            actualStream = new List<byte>();
            _output = new byte[0];
        }
        public MakeFile(byte[] preFormedBytes)
        {
            actualStream = new List<byte>();
            foreach(byte b in preFormedBytes)
            { actualStream.Add(b); }

            _output = actualStream.ToArray();
        }

        public void InsertByte(byte byteToInsert)
        {
            actualStream.Add(byteToInsert);
        }
        public void InsertBytes(byte[] byteToInsert)
        {
            foreach(byte b in byteToInsert) { actualStream.Add(b); }
        }
    }
}
