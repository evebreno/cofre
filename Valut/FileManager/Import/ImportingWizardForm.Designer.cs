﻿namespace Valut.FileManager.Import
{
    partial class ImportingWizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportingWizardForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.IncludeUserCB = new System.Windows.Forms.CheckBox();
            this.CopiesAllCB = new System.Windows.Forms.CheckBox();
            this.ValutDirL = new System.Windows.Forms.TextBox();
            this.SearchValutBT = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.CancelBT = new System.Windows.Forms.Button();
            this.SaveBT = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ComittedACDGV = new System.Windows.Forms.DataGridView();
            this.favoritoDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dominioDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usuarioDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CommittedBS = new System.Windows.Forms.BindingSource(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.UncomiitedAccDGV = new System.Windows.Forms.DataGridView();
            this.favoritoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dominioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usuarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncommittedBS = new System.Windows.Forms.BindingSource(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.CommitAllBT = new System.Windows.Forms.Button();
            this.UncomiitAllBT = new System.Windows.Forms.Button();
            this.UncommitSelectedBT = new System.Windows.Forms.Button();
            this.CommitSelectBT = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComittedACDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommittedBS)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UncomiitedAccDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UncommittedBS)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.IncludeUserCB);
            this.panel1.Controls.Add(this.CopiesAllCB);
            this.panel1.Controls.Add(this.ValutDirL);
            this.panel1.Controls.Add(this.SearchValutBT);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(877, 52);
            this.panel1.TabIndex = 0;
            // 
            // IncludeUserCB
            // 
            this.IncludeUserCB.AutoSize = true;
            this.IncludeUserCB.Location = new System.Drawing.Point(30, 27);
            this.IncludeUserCB.Name = "IncludeUserCB";
            this.IncludeUserCB.Size = new System.Drawing.Size(91, 17);
            this.IncludeUserCB.TabIndex = 2;
            this.IncludeUserCB.Text = "Incluir usuário";
            this.IncludeUserCB.UseVisualStyleBackColor = true;
            this.IncludeUserCB.CheckedChanged += new System.EventHandler(this.IncludeUserCB_CheckedChanged);
            // 
            // CopiesAllCB
            // 
            this.CopiesAllCB.AutoSize = true;
            this.CopiesAllCB.Location = new System.Drawing.Point(30, 4);
            this.CopiesAllCB.Name = "CopiesAllCB";
            this.CopiesAllCB.Size = new System.Drawing.Size(80, 17);
            this.CopiesAllCB.TabIndex = 2;
            this.CopiesAllCB.Text = "Copiar tudo";
            this.CopiesAllCB.UseVisualStyleBackColor = true;
            this.CopiesAllCB.CheckedChanged += new System.EventHandler(this.CopiesAllCB_CheckedChanged);
            // 
            // ValutDirL
            // 
            this.ValutDirL.Location = new System.Drawing.Point(202, 12);
            this.ValutDirL.Name = "ValutDirL";
            this.ValutDirL.ReadOnly = true;
            this.ValutDirL.Size = new System.Drawing.Size(165, 20);
            this.ValutDirL.TabIndex = 1;
            // 
            // SearchValutBT
            // 
            this.SearchValutBT.Location = new System.Drawing.Point(120, 10);
            this.SearchValutBT.Name = "SearchValutBT";
            this.SearchValutBT.Size = new System.Drawing.Size(75, 23);
            this.SearchValutBT.TabIndex = 0;
            this.SearchValutBT.Text = "Procurar";
            this.SearchValutBT.UseVisualStyleBackColor = true;
            this.SearchValutBT.Click += new System.EventHandler(this.SearchValutBT_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.UserNameLabel);
            this.panel2.Controls.Add(this.CancelBT);
            this.panel2.Controls.Add(this.SaveBT);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 374);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(877, 33);
            this.panel2.TabIndex = 1;
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Location = new System.Drawing.Point(13, 7);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(122, 13);
            this.UserNameLabel.TabIndex = 1;
            this.UserNameLabel.Text = "Usuário: NÃO INCLUSO";
            // 
            // CancelBT
            // 
            this.CancelBT.Location = new System.Drawing.Point(709, 7);
            this.CancelBT.Name = "CancelBT";
            this.CancelBT.Size = new System.Drawing.Size(75, 23);
            this.CancelBT.TabIndex = 0;
            this.CancelBT.Text = "Cancelar";
            this.CancelBT.UseVisualStyleBackColor = true;
            this.CancelBT.Click += new System.EventHandler(this.CancelBT_Click);
            // 
            // SaveBT
            // 
            this.SaveBT.Location = new System.Drawing.Point(790, 7);
            this.SaveBT.Name = "SaveBT";
            this.SaveBT.Size = new System.Drawing.Size(75, 23);
            this.SaveBT.TabIndex = 0;
            this.SaveBT.Text = "Salvar";
            this.SaveBT.UseVisualStyleBackColor = true;
            this.SaveBT.Click += new System.EventHandler(this.SaveBT_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ComittedACDGV);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(472, 52);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(405, 322);
            this.panel3.TabIndex = 2;
            // 
            // ComittedACDGV
            // 
            this.ComittedACDGV.AutoGenerateColumns = false;
            this.ComittedACDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ComittedACDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.favoritoDataGridViewCheckBoxColumn1,
            this.dominioDataGridViewTextBoxColumn1,
            this.usuarioDataGridViewTextBoxColumn1,
            this.obsDataGridViewTextBoxColumn1});
            this.ComittedACDGV.DataSource = this.CommittedBS;
            this.ComittedACDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComittedACDGV.Location = new System.Drawing.Point(0, 0);
            this.ComittedACDGV.MultiSelect = false;
            this.ComittedACDGV.Name = "ComittedACDGV";
            this.ComittedACDGV.ReadOnly = true;
            this.ComittedACDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ComittedACDGV.Size = new System.Drawing.Size(405, 287);
            this.ComittedACDGV.TabIndex = 0;
            // 
            // favoritoDataGridViewCheckBoxColumn1
            // 
            this.favoritoDataGridViewCheckBoxColumn1.DataPropertyName = "Favorito";
            this.favoritoDataGridViewCheckBoxColumn1.HeaderText = "Favorito";
            this.favoritoDataGridViewCheckBoxColumn1.Name = "favoritoDataGridViewCheckBoxColumn1";
            this.favoritoDataGridViewCheckBoxColumn1.ReadOnly = true;
            this.favoritoDataGridViewCheckBoxColumn1.Width = 50;
            // 
            // dominioDataGridViewTextBoxColumn1
            // 
            this.dominioDataGridViewTextBoxColumn1.DataPropertyName = "Dominio";
            this.dominioDataGridViewTextBoxColumn1.HeaderText = "Dominio";
            this.dominioDataGridViewTextBoxColumn1.Name = "dominioDataGridViewTextBoxColumn1";
            this.dominioDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // usuarioDataGridViewTextBoxColumn1
            // 
            this.usuarioDataGridViewTextBoxColumn1.DataPropertyName = "Usuario";
            this.usuarioDataGridViewTextBoxColumn1.HeaderText = "Usuario";
            this.usuarioDataGridViewTextBoxColumn1.Name = "usuarioDataGridViewTextBoxColumn1";
            this.usuarioDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // obsDataGridViewTextBoxColumn1
            // 
            this.obsDataGridViewTextBoxColumn1.DataPropertyName = "Obs";
            this.obsDataGridViewTextBoxColumn1.HeaderText = "Obs";
            this.obsDataGridViewTextBoxColumn1.Name = "obsDataGridViewTextBoxColumn1";
            this.obsDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // CommittedBS
            // 
            this.CommittedBS.DataSource = typeof(Valut.Accounts.Conta);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.UncomiitedAccDGV);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 52);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(472, 322);
            this.panel4.TabIndex = 3;
            // 
            // UncomiitedAccDGV
            // 
            this.UncomiitedAccDGV.AutoGenerateColumns = false;
            this.UncomiitedAccDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UncomiitedAccDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.favoritoDataGridViewCheckBoxColumn,
            this.dominioDataGridViewTextBoxColumn,
            this.usuarioDataGridViewTextBoxColumn,
            this.obsDataGridViewTextBoxColumn});
            this.UncomiitedAccDGV.DataSource = this.UncommittedBS;
            this.UncomiitedAccDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UncomiitedAccDGV.Location = new System.Drawing.Point(0, 0);
            this.UncomiitedAccDGV.MultiSelect = false;
            this.UncomiitedAccDGV.Name = "UncomiitedAccDGV";
            this.UncomiitedAccDGV.ReadOnly = true;
            this.UncomiitedAccDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.UncomiitedAccDGV.Size = new System.Drawing.Size(427, 287);
            this.UncomiitedAccDGV.TabIndex = 1;
            // 
            // favoritoDataGridViewCheckBoxColumn
            // 
            this.favoritoDataGridViewCheckBoxColumn.DataPropertyName = "Favorito";
            this.favoritoDataGridViewCheckBoxColumn.HeaderText = "Favorito";
            this.favoritoDataGridViewCheckBoxColumn.Name = "favoritoDataGridViewCheckBoxColumn";
            this.favoritoDataGridViewCheckBoxColumn.ReadOnly = true;
            this.favoritoDataGridViewCheckBoxColumn.Width = 50;
            // 
            // dominioDataGridViewTextBoxColumn
            // 
            this.dominioDataGridViewTextBoxColumn.DataPropertyName = "Dominio";
            this.dominioDataGridViewTextBoxColumn.HeaderText = "Dominio";
            this.dominioDataGridViewTextBoxColumn.Name = "dominioDataGridViewTextBoxColumn";
            this.dominioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // usuarioDataGridViewTextBoxColumn
            // 
            this.usuarioDataGridViewTextBoxColumn.DataPropertyName = "Usuario";
            this.usuarioDataGridViewTextBoxColumn.HeaderText = "Usuario";
            this.usuarioDataGridViewTextBoxColumn.Name = "usuarioDataGridViewTextBoxColumn";
            this.usuarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // obsDataGridViewTextBoxColumn
            // 
            this.obsDataGridViewTextBoxColumn.DataPropertyName = "Obs";
            this.obsDataGridViewTextBoxColumn.HeaderText = "Obs";
            this.obsDataGridViewTextBoxColumn.Name = "obsDataGridViewTextBoxColumn";
            this.obsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // UncommittedBS
            // 
            this.UncommittedBS.DataSource = typeof(Valut.Accounts.Conta);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.CommitAllBT);
            this.panel5.Controls.Add(this.UncomiitAllBT);
            this.panel5.Controls.Add(this.UncommitSelectedBT);
            this.panel5.Controls.Add(this.CommitSelectBT);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(427, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(45, 322);
            this.panel5.TabIndex = 0;
            // 
            // CommitAllBT
            // 
            this.CommitAllBT.Location = new System.Drawing.Point(6, 47);
            this.CommitAllBT.Name = "CommitAllBT";
            this.CommitAllBT.Size = new System.Drawing.Size(32, 23);
            this.CommitAllBT.TabIndex = 2;
            this.CommitAllBT.Text = ">>";
            this.CommitAllBT.UseVisualStyleBackColor = true;
            this.CommitAllBT.Click += new System.EventHandler(this.CommitAllBT_Click);
            // 
            // UncomiitAllBT
            // 
            this.UncomiitAllBT.Location = new System.Drawing.Point(6, 115);
            this.UncomiitAllBT.Name = "UncomiitAllBT";
            this.UncomiitAllBT.Size = new System.Drawing.Size(32, 23);
            this.UncomiitAllBT.TabIndex = 2;
            this.UncomiitAllBT.Text = "<<";
            this.UncomiitAllBT.UseVisualStyleBackColor = true;
            this.UncomiitAllBT.Click += new System.EventHandler(this.UncomiitAllBT_Click);
            // 
            // UncommitSelectedBT
            // 
            this.UncommitSelectedBT.Location = new System.Drawing.Point(6, 86);
            this.UncommitSelectedBT.Name = "UncommitSelectedBT";
            this.UncommitSelectedBT.Size = new System.Drawing.Size(32, 23);
            this.UncommitSelectedBT.TabIndex = 2;
            this.UncommitSelectedBT.Text = "<";
            this.UncommitSelectedBT.UseVisualStyleBackColor = true;
            this.UncommitSelectedBT.Click += new System.EventHandler(this.UncommitSelectedBT_Click);
            // 
            // CommitSelectBT
            // 
            this.CommitSelectBT.Location = new System.Drawing.Point(6, 18);
            this.CommitSelectBT.Name = "CommitSelectBT";
            this.CommitSelectBT.Size = new System.Drawing.Size(32, 23);
            this.CommitSelectBT.TabIndex = 2;
            this.CommitSelectBT.Text = ">";
            this.CommitSelectBT.UseVisualStyleBackColor = true;
            this.CommitSelectBT.Click += new System.EventHandler(this.CommitSelectBT_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "Valut.exe";
            this.openFileDialog1.Filter = "Cofre|Valut.exe|Executáveis|*.exe";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.Controls.Add(this.label1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 287);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(427, 35);
            this.panel6.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Coral;
            this.label1.Location = new System.Drawing.Point(141, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "NÃO CONFIRMADO";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 287);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(405, 35);
            this.panel7.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.YellowGreen;
            this.label2.Location = new System.Drawing.Point(161, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "CONFIRMADO";
            // 
            // ImportingWizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 407);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ImportingWizardForm";
            this.Text = "Assistente de importação de senhas [V1.0.0]";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ComittedACDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommittedBS)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UncomiitedAccDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UncommittedBS)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox IncludeUserCB;
        private System.Windows.Forms.CheckBox CopiesAllCB;
        private System.Windows.Forms.TextBox ValutDirL;
        private System.Windows.Forms.Button SearchValutBT;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button CancelBT;
        private System.Windows.Forms.Button SaveBT;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView ComittedACDGV;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView UncomiitedAccDGV;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button CommitAllBT;
        private System.Windows.Forms.Button UncomiitAllBT;
        private System.Windows.Forms.Button UncommitSelectedBT;
        private System.Windows.Forms.Button CommitSelectBT;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.BindingSource CommittedBS;
        private System.Windows.Forms.BindingSource UncommittedBS;
        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.DataGridViewCheckBoxColumn favoritoDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dominioDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuarioDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn obsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn favoritoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dominioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn obsDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
    }
}