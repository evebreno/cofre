﻿using System;
using System.Windows.Forms;

namespace Valut.FileManager.Import
{
    public partial class ImportingWizardForm : Form
    {
        internal string FileName { get; set; }
        internal bool HasAccount { get; set; }

        Accounts.Perfil ProfileInfo
        {
            get => _profileinfo;
            set
            {
                _profileinfo = value;
                UserNameLabel.Text = (ProfileInfo != null) ? ProfileInfo.Nome : "Usuário: NÃO INCLUSO";

            }
        }

        private Accounts.Perfil _profileinfo = null;

        public ImportingWizardForm()
        {
            InitializeComponent();
        }

        private void SearchValutBT_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                UncommittedBS.Clear();
                CommittedBS.Clear();

                ValutDirL.Text = FileName = openFileDialog1.FileName;
                var cofreFileName = ImportingAssistant.AdjustToCofre(FileName);
                PasswordGatherForm pgf = new PasswordGatherForm();
                using (var passowrd = pgf.GatherPassword())
                {
                    try
                    {
                        var contas = ImportingAssistant.LoadAccounts(cofreFileName, passowrd,
                            IncludeUserCB.Checked, out Accounts.Perfil profile);

                        if (CopiesAllCB.Checked)
                        {
                            foreach (var obj in contas) CommittedBS.Add(obj);
                        }

                        else foreach (var obj in contas) UncommittedBS.Add(obj);

                        ProfileInfo = (IncludeUserCB.Checked) ? profile : null;
                    }

                    catch(CheckingFailedException ex)
                    {
                        MessageBox.Show("Houve um erro na conferência de dados do " +
                            $"cofre a ser importado: {ex.Message}");
                    }

                    catch(Exception ex)
                    {
                        MessageBox.Show($"Não foi possível carregar as contas: {ex.Message}" +
                            $"\nO assistente irá fechar");
                        this.Close();
                    }
                }
            }
        }

        private void CopiesAllCB_CheckedChanged(object sender, EventArgs e)
        {
            if (CopiesAllCB.Checked) { IncludeUserCB.Checked = true; IncludeUserCB.Enabled = false; }
            else IncludeUserCB.Enabled = true;
        }

        private void CommitSelectBT_Click(object sender, EventArgs e)
        {
            var selectedObj = UncommittedBS.Current;
            CommittedBS.Add(selectedObj);
            UncommittedBS.Remove(selectedObj);
        }

        private void CommitAllBT_Click(object sender, EventArgs e)
        {
            foreach(var obj in UncommittedBS)
            {
                CommittedBS.Add(obj);
            }

            UncommittedBS.Clear();
        }

        private void UncommitSelectedBT_Click(object sender, EventArgs e)
        {
            var selectedObj = CommittedBS.Current;
            UncommittedBS.Add(selectedObj);
            CommittedBS.Remove(selectedObj);
        }

        private void UncomiitAllBT_Click(object sender, EventArgs e)
        {
            foreach (var obj in CommittedBS)
            {
                UncommittedBS.Add(obj);
            }

            CommittedBS.Clear();
        }

        private void SaveBT_Click(object sender, EventArgs e)
        {
            if(IncludeUserCB.Checked)
            {
                if(ProfileInfo != null)
                {
                    UserCenterData.PerfilData = ProfileInfo;
                }
                else MessageBox.Show("Usuário não carregado. O mesmo não será salvo.");
            }

            foreach(var newAcc in CommittedBS)
            {
                UserCenterData.AddAccount(newAcc as Accounts.Conta, false);
            }

            UserCenterData.SyncData();
            this.Close();
        }

        private void IncludeUserCB_CheckedChanged(object sender, EventArgs e)
        {
            if((UncommittedBS.Count != 0 || CommittedBS.Count !=0) && IncludeUserCB.Checked
                && ProfileInfo == null)
            {
                MessageBox.Show("Para incluir as informações de usuário é necessário reca" +
                    "rregar o arquivo.");
            }
        }

        private void CancelBT_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}