﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Security;
using Valut.Accounts;
using Valut.Encrypting;

namespace Valut.FileManager.Import
{
    internal class ImportingAssistant
    {
        /// <summary>
        /// Altera o endereço da string de valut.exe para .cofre
        /// </summary>
        /// <param name="applicationName">string a ser alterada</param>
        static internal string AdjustToCofre(string applicationName)
        {
            FileInfo file = new FileInfo(applicationName);
            var directory = file.Directory;
            var searchFile = from f in directory.EnumerateFiles()
                             where string.Equals(f.Name, ".cofre")
                             select f;


            string result = "";
            foreach(var resultFile in searchFile)
            { result = resultFile.FullName; break; }

            return result;
        }

        /// <summary>
        /// Carrega as contas do arquivo .cofre solicitado
        /// </summary>
        /// <param name="fileName">Arquivo .cofre a ser usado</param>
        /// <param name="loadProfile">Indica se deseja carregar os dados de perfil também</param>
        /// <param name="profile">Perfil a ser carregado</param>
        static internal Conta[] LoadAccounts(string fileName, SecureString password, bool loadProfile, out Perfil profile)
        {
            FileStream file = null;
            try
            {
                file = new FileStream(fileName, FileMode.Open);

                var directory = Diretorio.GetDirectoryInfo(new FileInfo(fileName).DirectoryName);

                if (!DoCheckIn(file, directory, password, out string reason))
                {
                    throw new CheckingFailedException(reason);
                }

                var key = PerfilThread.GetPK2(file, password, directory);
                var contas = AccountsThread.ObterContas(file, key.PrivateKey());
                if (loadProfile) profile = PerfilThread.ObterPerfil(file, password, directory);
                else profile = null;

                file.Close();
                return contas;
            }
            catch(Exception e)
            {
                file.Close();
                throw e;
            }
        }

        /// <summary>
        /// Faz todas as verificações necessárias para garantir integridade do arquivo
        /// </summary>
        /// <param name="file"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        static private bool DoCheckIn(FileStream file,Diretorio dir, SecureString password, out string reason)
        {
            try
            {
                if (BoundsThread.CompareProtocol(file) != 0)
                {
                    reason = "A versão do cofre selecionado é diferente do atual. Tente manter ambos" +
                        "na mesma verão";

                    return false;
                }
                var passCheck = BoundsThread.GetCheckPassword(file);

                if (!MD5Signer.VerifySignature(Encoding.Unicode.GetBytes(password.ShowString()), passCheck))
                {
                    reason = "A chave digitada não cofere.";
                    return false;
                }

                var direcCheck = BoundsThread.GetDirectorychecksum(file);
                if (!MD5Signer.VerifySignature(dir.GetDirectIdentifierData(), direcCheck))
                {
                    reason = "O diretório do cofre não é o original.";
                    return false;
                }

                reason = "Não foi encontrado inconsistencias no arquivo";
                return true;
            }

            catch(Exception ex)
            {
                reason = $"Houve um erro inesperado {ex.Message}";
                return false;
            }
        }
    }
}
