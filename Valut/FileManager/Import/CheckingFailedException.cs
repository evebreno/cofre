﻿using System;

namespace Valut.FileManager.Import
{
    internal class CheckingFailedException : Exception
    {
        internal new string Message { get; set; }
        
        internal CheckingFailedException(string message)
        {
            Message = message;
            HResult = 7410;
        }
    }
}
