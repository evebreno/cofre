﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Valut.Encrypting;

namespace Valut.FileManager
{
    [Serializable]
    public class Diretorio
    {
        public DriveType DType { get; set; }
        public string DriveName { get; set; }
        public string VolumeName { get; set; }
        public string DirectoryName { get; set; }
        public byte[] CheckSum;

        static public Diretorio GetCurrentDirectoryInfo()
        {
            string direct = $"{Directory.GetCurrentDirectory()}";
            DriveInfo[] directories = DriveInfo.GetDrives();
            Diretorio result = new Diretorio();

            foreach(var cdrive in directories)
            {
                if(direct.Contains(cdrive.Name))
                {
                    result.DriveName = cdrive.Name;
                    result.DType = cdrive.DriveType;
                    result.VolumeName = cdrive.VolumeLabel;
                    result.DirectoryName = direct;
                    result.GenerateChecksum();
                    break;
                }
            }

            if (result != null) { return result; }
            else throw new Exception("Erro inesperado!");
        }

        static public Diretorio GetDirectoryInfo(string direct)
        {
            DriveInfo[] directories = DriveInfo.GetDrives();
            Diretorio result = new Diretorio();

            foreach (var cdrive in directories)
            {
                if (direct.Contains(cdrive.Name))
                {
                    result.DriveName = cdrive.Name;
                    result.DType = cdrive.DriveType;
                    result.VolumeName = cdrive.VolumeLabel;
                    result.DirectoryName = direct;
                    result.GenerateChecksum();
                    break;
                }
            }

            if (result != null) { return result; }
            else throw new Exception("Erro inesperado!");
        }

        static private string NameOFDrive()
        {
            string direct = $"{Directory.GetCurrentDirectory()}";
            DriveInfo[] directories = DriveInfo.GetDrives();
            string result = "";
            foreach (var cdrive in directories)
            {
                if (direct.Contains(cdrive.Name)) { result = cdrive.Name; break; }
            }

            return result;
            
        }

        static private string NameOFDrive(string direct)
        {
            DriveInfo[] directories = DriveInfo.GetDrives();
            string result = "";
            foreach (var cdrive in directories)
            {
                if (direct.Contains(cdrive.Name)) { result = cdrive.Name; break; }
            }

            return result;

        }

        private void GenerateChecksum()
        {
            CheckSum = MD5Signer.GetSignature(GetDirectIdentifierData());
        }

        public byte[] GetDirectIdentifierData()
        {
            if (DType == DriveType.CDRom || DType == DriveType.Removable)
            {
                string name = DirectoryName.Replace(NameOFDrive(DirectoryName), "");
                List<byte> dataCollector = new List<byte>();
                dataCollector.AddRange(Encoding.Unicode.GetBytes(name));
                dataCollector.AddRange(Encoding.Unicode.GetBytes(VolumeName));

                return dataCollector.ToArray();
            }

            else
            {
                string name = DirectoryName;
                List<byte> dataCollector = new List<byte>();
                dataCollector.AddRange(Encoding.Unicode.GetBytes(name));
                dataCollector.AddRange(Encoding.Unicode.GetBytes(VolumeName));

                return dataCollector.ToArray();
            }
        }

        static public bool CompareDirectory(FileStream file, Diretorio directory)
        {
            try
            {
                var direcCheck = BoundsThread.GetDirectorychecksum(file);
                if (direcCheck.Length != directory.CheckSum.Length) return false;
                for (int i = 0; i < direcCheck.Length; i++)
                {
                    if (direcCheck[i] != directory.CheckSum[i]) return false;
                }

                return true;
            }

            catch (Exception ex) { throw ex; }
        }
    }
}
