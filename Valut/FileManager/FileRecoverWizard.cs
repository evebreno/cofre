﻿using System;
using System.IO;
using System.Security;
using Valut.Encrypting;
using System.Windows.Forms;

namespace Valut.FileManager
{
    static public class FileRecoverWizard
    {
        static public void RecoverFile(FileStream fs, SecureString password)
        {
            if (!CompareProtocol(fs)) throw new Exception("Não foi possível recuperar o arquivo");
            if (!CheckPassword(fs, password)) throw new Exception("Não foi possível recuperar o arquivo");
            if (!CheckDirectory(fs)) throw new Exception("Não foi possível recuperar o arquivo");
            if (!CheckPK2(fs, password)) throw new Exception("Não foi possível recuperar o arquivo");
            
            try
            {
                int failedAccounts = 0;
                var recovered = AccountsThread.ObterContasRecoverMode(fs, PerfilThread.GetPK2(fs, password).PrivateKey(), ref failedAccounts);
                foreach (var conta in recovered) UserCenterData.AddAccount(conta, false);
                MessageBox.Show($"Operação bem sucedida.\nContas não recuperadas: {failedAccounts}.");
            }
            catch (Exception e)
            {
                MessageBox.Show($"As contas não puderam ser recuperadas devido a um erro inesperado: {e.Message}.");
            }

            try { var profile = PerfilThread.ObterPerfil(fs, password); UserCenterData.PerfilData = profile; }
            catch {
                MessageBox.Show("As informações de perfil não puderam ser recuperadas. " +
                    "Será criado um usuário temporário para ser alterado depois nas configurações.");

                byte[] b = new byte[16];
                Random r = new Random();
                r.NextBytes(b);
                UserCenterData.PerfilData = new Accounts.Perfil(new Guid(b).ToString(), DateTime.Now);
            }

            
        }

        static private bool CompareProtocol(FileStream file)
        {
            try
            {
                switch (BoundsThread.CompareProtocol(file))
                {
                    case 0: return true;
                    case 1:
                        if (MessageBox.Show("O protocolo de cofre a ser " +
                            "lido é obsoleto, mas ainda é possível tentar recuperar algumas " +
                            "informações do mesmo, deseja prosseguir?", "Cofre obsoleto",
                            MessageBoxButtons.YesNo) == DialogResult.Yes)
                        { return true; }

                        else return false;

                    case -1:
                        MessageBox.Show("O cofre lido utiliza uma versão posterior e, portanto " +
                            "não é possível prossegir com a leitura do mesmo. Tente obter um cofre atualizado");
                        return false;

                    default:
                        MessageBox.Show("Resultado inesperado!");
                        return false;
                }
            }
            catch
            {
                if (MessageBox.Show("A versão do cofre não pôde ser verificada. " +
                    "Isto pode ocorrer quando " +
                    "o arquivo está danificado demais para a conferência. Ainda " +
                    "assim deseja prosseguir?", "Protocolo não verificado", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return true;
                }

                else return false;
            }
        }

        static private bool CheckPassword(FileStream file, SecureString password)
        {
            try
            {
                if (!PerfilThread.ComparePass(file, password))
                {
                    return ShowMessage();
                }
                return true;
            }

            catch { return ShowMessage(); }

            bool ShowMessage()
            {
                if (MessageBox.Show("A senha inserida não confere com a armazenada. " +
                    "Isto pode ocorrer quando a senha está incorreta, ou quando " +
                    "o arquivo está danificado demais para a conferência. Ainda " +
                    "assim deseja prosseguir?", "Senha não verificada", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return true;
                }

                else return false;
            }
        }

        static private bool CheckDirectory(FileStream file)
        {
            try
            {
                if (!Diretorio.CompareDirectory(file, Diretorio.GetCurrentDirectoryInfo()))
                {
                    return ShowMessage();
                }
                return true;
            }
            catch
            {
                return ShowMessage();
            }

            bool ShowMessage()
            {
                if (MessageBox.Show("O diretório avaliado não confere com o armazenado. " +
                        "Isto pode ocorrer quando o cofre está em outro diretório, ou quando " +
                        "o arquivo está danificado demais para a conferência. Ainda " +
                        "assim deseja prosseguir?", "Diretório não verificado", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return true;
                }

                else return false;
            }
        }

        static private bool CheckPK2(FileStream file, SecureString password)
        {
            try
            {
                var data = BoundsThread.GetCheckPK2(file);
                var key = PerfilThread.GetPK2(file, password);

                if (!MD5Signer.VerifySignature(key.PrivateKey().Key(), data))
                {
                    return ShowMessage();
                }

                return true;
            }

            catch(System.Security.Cryptography.CryptographicException)
            {
                MessageBox.Show("Lamento, mas houve um erro irreparável na senha " +
                    "interna, e não é possível recuperá-la. Verifique se a chave " +
                    "digitada está correta, e tente novamente.");

                Environment.Exit(0);

                return false;
            }

            catch(Exception)
            {
                return ShowMessage();
            }

            bool ShowMessage()
            {
                if (MessageBox.Show("A senha interna armazenada não passou no teste. " +
                    "Isto pode ocorrer quando ela foi violada, ou quando " +
                    "o arquivo está danificado demais para a conferência. Ainda " +
                    "assim deseja prosseguir?", "Senha interna incorreta", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    return true;
                }

                else return false;
            }
        }

    }
}
