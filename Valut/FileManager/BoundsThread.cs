﻿using System;
using Valut.Properties;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Valut.FileManager
{
    /// <summary>
    /// Fornece métodos estáticos para trabalhar com as informações de cabeçalho e rodapé
    /// </summary>
    static public class BoundsThread
    {
        /// <summary>
        /// Compara o procolo de arquivo atual com o protocolo presente no arquivo salvo.
        /// </summary>
        /// <returns>0 se são iguais, 1 se o protocolo atual é posterior e -1 se for inferior</returns>
        static public int CompareProtocol(FileStream file)
        {
            file.Position = 0;
            byte[] buff = new byte[file.Length];
            file.Read(buff, 0, buff.Length);

            ReadFile rf = new ReadFile(buff);
            rf.ReadUntil(new byte[] { 0, 0, 1, 1, 30, 70, 255, 255 },false);
            MemoryStream protocol = new MemoryStream(rf.ReadUntil(new byte[] { 0, 0, 1, 1, 31, 70, 255, 255 }, true));

            BinaryFormatter udb = new BinaryFormatter();
            Protocolo protocolo = udb.Deserialize(protocol) as Protocolo;


            int maj = int.Parse(Resources.Cofre_Major);
            int min = int.Parse(Resources.Cofre_Minor);
            int rev = int.Parse(Resources.Cofre_Revision);
            int resultado = 0;

            if ((resultado = maj.CompareTo(protocolo.Major)) != 0)
                goto RETURNING;

            else if ((resultado = min.CompareTo(protocolo.Minor)) != 0)
                goto RETURNING;

            else if ((resultado = rev.CompareTo(protocolo.Revisão)) != 0)
                goto RETURNING;


                RETURNING:;
            return resultado;
        }

        static public byte[] GetDirectorychecksum(FileStream file)
        {
            file.Position = 0;
            byte[] buff = new byte[file.Length];
            file.Read(buff, 0, buff.Length);

            ReadFile rf = new ReadFile(buff);
            rf.ReadUntil(new byte[] { 0, 0, 1, 1, 30, 72, 255, 255 }, false);
            MemoryStream directoryM = new MemoryStream(rf.ReadUntil(new byte[] { 0, 0, 1, 1, 31, 72, 255, 255 }, true));
            try
            {
                BinaryFormatter udb = new BinaryFormatter();
                Diretorio diretorio = udb.Deserialize(directoryM) as Diretorio;

                return diretorio.CheckSum;
            }

            catch(Exception ex) { throw ex; }
        }

        static public byte[] GetCheckPassword(FileStream file)
        {
            file.Position = 0;
            byte[] buff = new byte[file.Length];
            file.Read(buff, 0, buff.Length);

            ReadFile rf = new ReadFile(buff);
            rf.ReadUntil(new byte[] { 0, 0, 5, 7, 6, 4, 255, 255 }, false);
            byte[] passbuff = rf.ReadUntil(new byte[] { 0, 0, 6, 7, 6, 4, 255, 255 }, true);

            return passbuff;
        }

        static public byte[] GetCheckPK2(FileStream file)
        {
            file.Position = 0;
            byte[] buff = new byte[file.Length];
            file.Read(buff, 0, buff.Length);

            ReadFile rf = new ReadFile(buff);
            rf.ReadUntil(new byte[] { 0, 0, 50, 70, 6, 4, 255, 255 }, false);
            byte[] Pk2 = rf.ReadUntil(new byte[] { 0, 0, 60, 70, 6, 4, 255, 255 }, true);

            return Pk2;
        }
    }
}
