﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Schema;
using Valut.Accounts;
using Valut.Encrypting;
using System.Security;
using System.Security.Cryptography;

namespace Valut.FileManager
{
    /// <summary>
    /// Realiza as operações de leitura e escrita do perfil no disco
    /// </summary>
    public class PerfilThread
    {
        static public Perfil ObterPerfil(FileStream file, SecureString password)
        {
            file.Position = 0;
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            ReadFile reader = new ReadFile(buffer);

            reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 }, false);
            byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 }, true);
            reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 }, false);
            byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 }, true);

            try
            {
                List<byte> passkey = new List<byte>();
                passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());

                BinaryFormatter format = new BinaryFormatter();
                byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

                MemoryStream ms = new MemoryStream(decryptedData);
                return format.Deserialize(ms) as Perfil;
            }
            catch(Exception e) { throw e; }
        }

        static public Perfil ObterPerfil(FileStream file, SecureString password, Diretorio directory)
        {
            file.Position = 0;
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            ReadFile reader = new ReadFile(buffer);

            reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 }, false);
            byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 }, true);
            reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 }, false);
            byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 }, true);

            try
            {
                List<byte> passkey = new List<byte>();
                passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                passkey.AddRange(directory.GetDirectIdentifierData());

                BinaryFormatter format = new BinaryFormatter();
                byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

                MemoryStream ms = new MemoryStream(decryptedData);
                return format.Deserialize(ms) as Perfil;
            }
            catch (Exception e) { throw e; }
        }

        /// <summary>
        /// Obtém a chave pública.
        /// </summary>
        static public RSAParameters GetPK1(FileStream file, SecureString password)
        {
            file.Position = 0;
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            ReadFile reader = new ReadFile(buffer);

            reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 }, false);
            byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 }, true);
            reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 }, false);
            byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 }, true);

            List<byte> passkey = new List<byte>();
            passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
            passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());

            BinaryFormatter format = new BinaryFormatter();
            byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

            MemoryStream ms = new MemoryStream(decryptedData);
            RSAParameters result = (RSAParameters)format.Deserialize(ms);
            return result;
        }

        static public RSAParameters GetPK1(SecureString password)
        {
            FileStream file = null;
            try{
                file = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open)
                {
                    Position = 0
                };
                byte[] buffer = new byte[file.Length];
                file.Read(buffer, 0, buffer.Length);
                ReadFile reader = new ReadFile(buffer);

                reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 }, false);
                byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 }, true);
                reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 }, false);
                byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 }, true);

                List<byte> passkey = new List<byte>();
                passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());

                BinaryFormatter format = new BinaryFormatter();
                byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

                MemoryStream ms = new MemoryStream(decryptedData);
                RSAParameters result = (RSAParameters)format.Deserialize(ms);
                file.Close();
                return result;
            }
            catch(Exception e){
                file.Close();
                throw e;
            }
        }

        /// <summary>
        /// Obtém a chave privada
        /// </summary
        static public RSAContainerData GetPK2(FileStream file, SecureString password)
        {
            file.Position = 0;
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            ReadFile reader = new ReadFile(buffer);
            try
            {
                reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }, false);
                byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }, true);
                reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }, false);
                byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }, true);

                List<byte> passkey = new List<byte>();
                passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());

                BinaryFormatter format = new BinaryFormatter();
                byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

                MemoryStream ms = new MemoryStream(decryptedData);
                return format.Deserialize(ms) as RSAContainerData;
            }

            catch(Exception ex) { throw ex; }
        }

        static public RSAContainerData GetPK2(FileStream file, SecureString password, Diretorio direcInfo)
        {
            file.Position = 0;
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            ReadFile reader = new ReadFile(buffer);
            try
            {
                reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }, false);
                byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }, true);
                reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }, false);
                byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }, true);

                List<byte> passkey = new List<byte>();
                passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                passkey.AddRange(direcInfo.GetDirectIdentifierData());

                BinaryFormatter format = new BinaryFormatter();
                byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

                MemoryStream ms = new MemoryStream(decryptedData);
                return format.Deserialize(ms) as RSAContainerData;
            }

            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Obtém a chave privada
        /// </summary
        static public RSAContainerData GetPK2(SecureString password)
        {
            FileStream file = null;
            try{
                file = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open)
                {
                    Position = 0
                };
                byte[] buffer = new byte[file.Length];
                file.Read(buffer, 0, buffer.Length);
                ReadFile reader = new ReadFile(buffer);

                reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }, false);
                byte[] flux = reader.ReadUntil(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }, true);
                reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }, false);
               byte[] IV = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }, true);

                List<byte> passkey = new List<byte>();
                passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());

                BinaryFormatter format = new BinaryFormatter();
                byte[] decryptedData = AESCrypter.DecryptData(flux, passkey.ToArray(), IV);

                MemoryStream ms = new MemoryStream(decryptedData);
                file.Close();
                return format.Deserialize(ms) as RSAContainerData;
            }
            
            catch(Exception e)
            {
                file.Close();
                throw e;
            }
        }

        static public bool ValidatePerfilSign(FileStream file)
        {
            file.Position = 0;
            byte[] buffer = new byte[file.Length];
            file.Read(buffer, 0, buffer.Length);
            ReadFile reader = new ReadFile(buffer);

            int designatedPosition = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 6, 255, 255 }, false).Length+8;
            byte[] signer = reader.ReadUntil(new byte[] { 0, 0, 7, 8, 9, 6, 255, 255 }, true);

            List<byte> dataToVerify = new List<byte>();
            for(int i=0;i<designatedPosition; i++)
            {
                dataToVerify.Add(buffer[i]);
            }

            return SHA512Signer.VerifySignature(dataToVerify.ToArray(), signer);
        }

        static void ValidationErrors(object sender, ValidationEventArgs e)
        {

        }

        static public bool ComparePass(FileStream file, SecureString password)
        {
            var passcheck = BoundsThread.GetCheckPassword(file);
            return MD5Signer.VerifySignature(Encoding.Unicode.GetBytes(password.ShowString()), passcheck);
        }

        static public bool ComparePass(SecureString password)
        {
            FileStream file = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open)
            {
                Position = 0
            };

            var passcheck = BoundsThread.GetCheckPassword(file);
            file.Close();

            return MD5Signer.VerifySignature(Encoding.Unicode.GetBytes(password.ShowString()), passcheck);
        }
    }
}
