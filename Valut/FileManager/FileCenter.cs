﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Valut.Accounts;
using Valut.Encrypting;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Valut.Properties;


namespace Valut.FileManager
{
    static public class FileCenter
    {
        static public bool CheckPerfil()
        {
            using (FileStream fs = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open))
            {
                bool result = PerfilThread.ValidatePerfilSign(fs);
                fs.Close();
                return result;
            }
        }

        static public Perfil RetrievePerfil(SecureString password)
        {
            using (FileStream fs = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open))
            {
                Perfil p = PerfilThread.ObterPerfil(fs, password);
                fs.Close();
                return p;
            }
        }

        ///// <summary>
        ///// Obtem os dados da conta e as acrescenta em <see cref="UserCenterData"/>.
        ///// </summary>
        ///// <param name="key">Chave para descriptografar os dados.</param>
        //static public void RetrieveAccounts(RSAContainerData key)
        //{
        //    FileStream main = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open);
        //    foreach(var conta in AccountsThread.ObterContas(main, key))
        //    {
        //        UserCenterData.AddAccount(conta, false);
        //    }
        //}

        static public void CriarPerfil(string nome, SecureString password)
        {
            Perfil usuario = new Perfil(nome, DateTime.Now, 5);
            using (FileStream fs = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Create))
            {
                RSAParameters key = RSACrypter.GenerateKey();
                RSAContainerData privatekey = new RSAContainerData(key);

                UpdateHeader(fs);

                using (MemoryStream userData = new MemoryStream())
                {
                    BinaryFormatter udb = new BinaryFormatter();
                    udb.Serialize(userData, usuario);

                    using (MemoryStream ms1 = new MemoryStream())
                    {
                        BinaryFormatter bf1 = new BinaryFormatter();
                        bf1.Serialize(ms1, privatekey);

                        using (MemoryStream ms2 = new MemoryStream())
                        {
                            BinaryFormatter bf2 = new BinaryFormatter();
                            bf1.Serialize(ms2, key);

                            byte[] buffer = new byte[userData.GetBuffer().Length + ms1.GetBuffer().Length + ms2.GetBuffer().Length];
                            userData.GetBuffer().CopyTo(buffer, 0);
                            ms1.GetBuffer().CopyTo(buffer, userData.GetBuffer().Length);
                            ms2.GetBuffer().CopyTo(buffer, userData.GetBuffer().Length + ms1.GetBuffer().Length);
                            AesManaged msn = new AesManaged()
                            {
                                Mode = CipherMode.CBC,
                                Padding = PaddingMode.PKCS7
                            };

                            msn.GenerateIV();
                            byte[] IV1 = msn.IV;
                            msn.GenerateIV();
                            byte[] IV2 = msn.IV;
                            msn.GenerateIV();
                            byte[] IV3 = msn.IV;

                            List<byte> passkey = new List<byte>();
                            passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
                            passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());

                            byte[] result = AESCrypter.EncryptData(userData.GetBuffer(), passkey.ToArray(), IV1);
                            MakeFile mf = new MakeFile();
                            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 }); //USUARIO
                            mf.InsertBytes(result);
                            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 });

                            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 }); //PK1
                            mf.InsertBytes(AESCrypter.EncryptData(ms2.GetBuffer(), passkey.ToArray(), IV2));
                            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 });

                            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }); //PK2
                            mf.InsertBytes(AESCrypter.EncryptData(ms1.GetBuffer(), passkey.ToArray(), IV3));
                            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 });

                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 }); //IV1
                            mf.InsertBytes(IV1);
                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 });

                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 }); //IV2
                            mf.InsertBytes(IV2);
                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 });

                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }); //IV3
                            mf.InsertBytes(IV3);
                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 });

                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 6, 255, 255 }); //ASSINATURA
                            mf.InsertBytes(SHA512Signer.GetSignature(mf.Output));
                            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 6, 255, 255 });

                            fs.Write(mf.Output, 0, mf.Output.Length);

                            UpdateFooter(fs, password, privatekey.PrivateKey());
                            fs.Close();
                            File.SetAttributes(fs.Name, FileAttributes.Hidden);
                        }
                    }
                }
            }
        }
        
        static public void UpdateFile(Perfil perfil, Conta[] contas, SecureString password)
        {
            string fileName = $"{Directory.GetCurrentDirectory()}/.cofre";
            File.SetAttributes(fileName, FileAttributes.Normal);
            using (FileStream main = new FileStream(fileName, FileMode.Create))
            {
                UpdateHeader(main);
                var newKey = UpdateProfile(perfil, main, password);
                UpdateAccounts(contas, main, newKey);
                UpdateFooter(main, password, newKey);

                main.Close();
                main.Dispose();
                File.SetAttributes(fileName, FileAttributes.Hidden);
            }
        }

        static private void UpdateHeader(FileStream file)
        {
            int maj = int.Parse(Resources.Cofre_Major);
            int min = int.Parse(Resources.Cofre_Minor);
            int rev = int.Parse(Resources.Cofre_Revision);
            var date = DateTime.Parse(Resources.Cofre_Date);

            Protocolo prot = new Protocolo(maj, min, rev, date);
            Diretorio direc = Diretorio.GetCurrentDirectoryInfo();
            MemoryStream[] memories = new MemoryStream[2];
            BinaryFormatter udb = new BinaryFormatter();

            for(int i = 0; i < memories.Length; i++) { memories[i] = new MemoryStream(); }
            udb.Serialize(memories[0], prot);
            udb.Serialize(memories[1], direc);

            MakeFile mf = new MakeFile();
            mf.InsertBytes(new byte[] { 0, 0, 1, 1, 30, 70, 255, 255 });
            mf.InsertBytes(memories[0].GetBuffer());
            mf.InsertBytes(new byte[] { 0, 0, 1, 1, 31, 70, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 1, 1, 30, 72, 255, 255 });
            mf.InsertBytes(memories[1].GetBuffer());
            mf.InsertBytes(new byte[] { 0, 0, 1, 1, 31, 72, 255, 255 });

            file.Write(mf.Output, 0, mf.Output.Length);
            foreach(var m in memories) { m.Dispose(); }
        }

        static private RSAParameters UpdateProfile(Perfil perfil, FileStream file, SecureString password)
        {
            MemoryStream userData = new MemoryStream();
            BinaryFormatter udb = new BinaryFormatter();
            udb.Serialize(userData, perfil);
            RSAParameters key = RSACrypter.GenerateKey();
            RSAContainerData privatekey = new RSAContainerData(key);

            MemoryStream ms1 = new MemoryStream();
            BinaryFormatter bf1 = new BinaryFormatter();
            bf1.Serialize(ms1, key);

            MemoryStream ms2 = new MemoryStream();
            BinaryFormatter bf2 = new BinaryFormatter();
            bf1.Serialize(ms2, privatekey);

            byte[] buffer = new byte[userData.GetBuffer().Length + ms1.GetBuffer().Length + ms2.GetBuffer().Length];
            userData.GetBuffer().CopyTo(buffer, 0);
            ms1.GetBuffer().CopyTo(buffer, userData.GetBuffer().Length);
            ms2.GetBuffer().CopyTo(buffer, userData.GetBuffer().Length + ms1.GetBuffer().Length);
            AesManaged msn = new AesManaged()
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            msn.GenerateIV();
            byte[] IV1 = msn.IV;
            msn.GenerateIV();
            byte[] IV2 = msn.IV;
            msn.GenerateIV();
            byte[] IV3 = msn.IV;

            List<byte> passkey = new List<byte>();
            passkey.AddRange(Encoding.Unicode.GetBytes(password.ShowString()));
            passkey.AddRange(Diretorio.GetCurrentDirectoryInfo().GetDirectIdentifierData());
            byte[] result = AESCrypter.EncryptData(userData.GetBuffer(), passkey.ToArray(), IV1);

            MakeFile mf = new MakeFile();
            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 }); //USUARIO
            mf.InsertBytes(result);
            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 2, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 }); //PK1
            mf.InsertBytes(AESCrypter.EncryptData(ms1.GetBuffer(), passkey.ToArray(), IV2));
            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 1, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 }); //PK2
            mf.InsertBytes(AESCrypter.EncryptData(ms2.GetBuffer(), passkey.ToArray(), IV3));
            mf.InsertBytes(new byte[] { 0, 0, 7, 9, 0, 0, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 }); //IV1
            mf.InsertBytes(IV1);
            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 9, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 }); //IV2
            mf.InsertBytes(IV2);
            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 8, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 }); //IV3
            mf.InsertBytes(IV3);
            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 7, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 6, 255, 255 }); //ASSINATURA
            mf.InsertBytes(SHA512Signer.GetSignature(mf.Output));
            mf.InsertBytes(new byte[] { 0, 0, 7, 8, 9, 6, 255, 255 });

            file.Write(mf.Output, 0, mf.Output.Length);
            return key;
        }

        static private void UpdateAccounts(Conta[] contas, FileStream file, RSAParameters key)
        {
            foreach(var conta in contas)
            {
                UpdateSingleAccount(conta, file, key);
            }
        }

        static private void UpdateSingleAccount(IContaMain conta, FileStream file, RSAParameters key)
        {
            MemoryStream accountData = new MemoryStream();
            BinaryFormatter accountFormatter = new BinaryFormatter();
            accountFormatter.Serialize(accountData, conta.GetSerializableAccount(key));

            byte[] hash = SHA512Signer.GetSignature(Encoding.Unicode.GetBytes(conta.Senha.ShowString()));

            AesManaged msn = new AesManaged()
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };
            msn.GenerateIV();
            byte[] IV1 = msn.IV;
            
            byte[] result = AESCrypter.EncryptData(accountData.GetBuffer(), key.Key(), IV1);
            byte[] mix = new byte[result.Length + hash.Length];

            result.CopyTo(mix, 0);
            hash.CopyTo(mix, result.Length);
            var sign = SHA512Signer.GetSignature(mix);

            MakeFile mf = new MakeFile();
            mf.InsertBytes(new byte[] { 0, 0, 60, 70, 255, 255 });
            mf.InsertBytes(result);
            mf.InsertBytes(new byte[] { 0, 0, 61, 70, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 60, 71, 255, 255 });
            mf.InsertBytes(hash);
            mf.InsertBytes(new byte[] { 0, 0, 61, 71, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 60, 72, 255, 255 });
            mf.InsertBytes(sign);
            mf.InsertBytes(new byte[] { 0, 0, 61, 72, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 60, 73, 255, 255 });
            mf.InsertBytes(IV1);
            mf.InsertBytes(new byte[] { 0, 0, 61, 73, 255, 255 });

            foreach (var b in mf.Output) file.WriteByte(b);
        }

        static private void UpdateFooter(FileStream file, SecureString password, RSAParameters key)
        {
            MakeFile mf = new MakeFile();
            mf.InsertBytes(new byte[] { 0, 0, 5, 7, 6, 4, 255, 255 });
            mf.InsertBytes(MD5Signer.GetSignature(Encoding.Unicode.GetBytes(password.ShowString())));
            mf.InsertBytes(new byte[] { 0, 0, 6, 7, 6, 4, 255, 255 });

            mf.InsertBytes(new byte[] { 0, 0, 50, 70, 6, 4, 255, 255 });
            mf.InsertBytes(MD5Signer.GetSignature(key.Key()));
            mf.InsertBytes(new byte[] { 0, 0, 60, 70, 6, 4, 255, 255 });

            mf.InsertBytes(new byte[] { 254, 254, 254, 254, 254, 254, 254, 254 });

            file.Write(mf.Output, 0, mf.Output.Length);
        }

        static private void TryResult(byte[] tryRead, byte[] key, byte[] iv)
        {
            byte[] result = AESCrypter.DecryptData(tryRead, key, iv);
            MemoryStream ms = new MemoryStream(result);
            BinaryFormatter bf = new BinaryFormatter();
            Perfil p = bf.Deserialize(ms) as Perfil;

            MessageBox.Show($"Nome:{p.Nome}\nCriado em: {p.Criação}\nModificado em: {p.Modificação}");
        }
    }
}
