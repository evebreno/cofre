﻿using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;
using Valut.Accounts;
using Valut.Encrypting;

namespace Valut.FileManager
{
    /// <summary>
    /// Realiza as operações de leitura da conta no disco
    /// </summary>
    public class AccountsThread
    {
        /// <summary>
        /// Obtém as contas salvas com a senha criptografada
        /// </summary>
        /// <param name="file">fluxo a ser usado</param>
        /// <param name="key">Chave utilizada</param>
        static public Conta[] ObterContas(FileStream file, RSAParameters key)
        {
            file.Position = 0;
            byte[] fileData = new byte[file.Length];
            file.Read(fileData, 0, (int)file.Length);
            ReadFile rf = new ReadFile(fileData);
            List<Conta> contas = new List<Conta>();
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms;

            while (rf.ActualPosition+1 < fileData.Length)
            {
                rf.ReadUntil(new byte[] { 0, 0, 60, 70, 255, 255 }, false);
                byte[] criticalData = rf.ReadUntil(new byte[] { 0, 0, 61, 70, 255, 255 }, true);

                rf.ReadUntil(new byte[] { 0, 0, 60, 71, 255, 255 }, false);
                byte[] hash = rf.ReadUntil(new byte[] { 0, 0, 61, 71, 255, 255 }, true);

                rf.ReadUntil(new byte[] { 0, 0, 60, 72, 255, 255 }, false);
                byte[] sign = rf.ReadUntil(new byte[] { 0, 0, 61, 72, 255, 255 }, true);

                rf.ReadUntil(new byte[] { 0, 0, 60, 73, 255, 255 }, false);
                byte[] IV1 = rf.ReadUntil(new byte[] { 0, 0, 61, 73, 255, 255 }, true);

                if (criticalData.Length != 0)
                {
                    byte[] result = AESCrypter.DecryptData(criticalData, key.Key(), IV1);
                    ms = new MemoryStream(result);
                    var tmpacc = bf.Deserialize(ms) as SerializableConta;
                    var tmpnacc = tmpacc.GetNonSerializableAccount(key);
                    tmpnacc.Hash = hash;
                    tmpnacc.Assinatura = sign;

                    tmpnacc.DecryptPassword(key);
                    if (ms.Length != 0)
                    {
                        if (SHA512Signer.VerifySignature(Encoding.Unicode.GetBytes(tmpnacc.Senha.ShowString()), hash))
                        {
                            tmpnacc.EncryptPassword(key);
                            contas.Add(tmpnacc);
                        }
                        else throw new System.Exception($"Integridade corrompida de senha:" +
                            $" A conta {tmpnacc.Usuario} do domínio {tmpnacc.Dominio} não passou " +
                            $"no teste de verificação de senha, o que pode indicar uma possível fraude.");
                    }
                }
                else return contas.ToArray();
            }

            return contas.ToArray();
        }

        static public Conta[] ObterContasRecoverMode(FileStream file, RSAParameters key, ref int failedOp)
        {
            file.Position = 0;
            byte[] fileData = new byte[file.Length];
            file.Read(fileData, 0, (int)file.Length);
            ReadFile rf = new ReadFile(fileData);
            List<Conta> contas = new List<Conta>();
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms;

            while (rf.ActualPosition + 1 < fileData.Length)
            {
                rf.ReadUntil(new byte[] { 0, 0, 60, 70, 255, 255 }, false);
                byte[] criticalData = rf.ReadUntil(new byte[] { 0, 0, 61, 70, 255, 255 }, true);

                rf.ReadUntil(new byte[] { 0, 0, 60, 71, 255, 255 }, false);
                byte[] hash = rf.ReadUntil(new byte[] { 0, 0, 61, 71, 255, 255 }, true);

                rf.ReadUntil(new byte[] { 0, 0, 60, 72, 255, 255 }, false);
                byte[] sign = rf.ReadUntil(new byte[] { 0, 0, 61, 72, 255, 255 }, true);

                rf.ReadUntil(new byte[] { 0, 0, 60, 73, 255, 255 }, false);
                byte[] IV1 = rf.ReadUntil(new byte[] { 0, 0, 61, 73, 255, 255 }, true);

                if (criticalData.Length != 0)
                {
                    try
                    {
                        byte[] result = AESCrypter.DecryptData(criticalData, key.Key(), IV1);
                        ms = new MemoryStream(result);
                        var tmpacc = bf.Deserialize(ms) as SerializableConta;
                        var tmpnacc = tmpacc.GetNonSerializableAccount(key);
                        tmpnacc.Hash = hash;
                        tmpnacc.Assinatura = sign;

                        tmpnacc.DecryptPassword(key);
                        if (ms.Length != 0)
                        {
                            if (SHA512Signer.VerifySignature(Encoding.Unicode.GetBytes(tmpnacc.Senha.ShowString()), hash))
                            {
                                tmpnacc.EncryptPassword(key);
                                contas.Add(tmpnacc);
                            }
                            else throw new System.Exception($"Integridade corrompida de senha:" +
                                $" A conta {tmpnacc.Usuario} do domínio {tmpnacc.Dominio} não passou " +
                                $"no teste de verificação de senha, o que pode indicar uma possível fraude.");
                        }
                    }
                    catch { failedOp++; }

                }
                else return contas.ToArray();
            }

            return contas.ToArray();
        }
    }
}
