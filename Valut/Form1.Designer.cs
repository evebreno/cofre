﻿namespace Valut
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.UsuarioRBT = new System.Windows.Forms.RadioButton();
            this.DominioRBT = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.FiltarFavoritosBT = new System.Windows.Forms.Button();
            this.FiltragemTB = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LaunchImportingWizardBT = new System.Windows.Forms.Button();
            this.SobreBT = new System.Windows.Forms.Button();
            this.UsuarioBT = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ChecarIntegridadeBT = new System.Windows.Forms.Button();
            this.AssinaturaTB = new System.Windows.Forms.TextBox();
            this.HashTB = new System.Windows.Forms.TextBox();
            this.IntegridadeTB = new System.Windows.Forms.TextBox();
            this.PassTB = new System.Windows.Forms.TextBox();
            this.ModificadoEmTB = new System.Windows.Forms.TextBox();
            this.CriadoEmTB = new System.Windows.Forms.TextBox();
            this.UsuarioTB = new System.Windows.Forms.TextBox();
            this.DominioTB = new System.Windows.Forms.TextBox();
            this.RevelarSenhaBt = new System.Windows.Forms.Button();
            this.CopiarSenhaBT = new System.Windows.Forms.Button();
            this.AssinaturaLB = new System.Windows.Forms.Label();
            this.HashLB = new System.Windows.Forms.Label();
            this.IntegridadeLB = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.FavoritarBT = new System.Windows.Forms.Button();
            this.ShowAdvDataBT = new System.Windows.Forms.Button();
            this.ExcluirContaBT = new System.Windows.Forms.Button();
            this.ConfigSenhaBT = new System.Windows.Forms.Button();
            this.NovaSenha = new System.Windows.Forms.Button();
            this.VisualizadorContas = new System.Windows.Forms.DataGridView();
            this.favoritoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.obsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Dica = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VisualizadorContas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.FiltarFavoritosBT);
            this.panel1.Controls.Add(this.FiltragemTB);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(684, 80);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.UsuarioRBT);
            this.groupBox1.Controls.Add(this.DominioRBT);
            this.groupBox1.Location = new System.Drawing.Point(10, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 42);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // UsuarioRBT
            // 
            this.UsuarioRBT.AutoSize = true;
            this.UsuarioRBT.Location = new System.Drawing.Point(132, 19);
            this.UsuarioRBT.Name = "UsuarioRBT";
            this.UsuarioRBT.Size = new System.Drawing.Size(61, 17);
            this.UsuarioRBT.TabIndex = 11;
            this.UsuarioRBT.TabStop = true;
            this.UsuarioRBT.Text = "Usuário";
            this.UsuarioRBT.UseVisualStyleBackColor = true;
            // 
            // DominioRBT
            // 
            this.DominioRBT.AutoSize = true;
            this.DominioRBT.Checked = true;
            this.DominioRBT.Location = new System.Drawing.Point(36, 19);
            this.DominioRBT.Name = "DominioRBT";
            this.DominioRBT.Size = new System.Drawing.Size(65, 17);
            this.DominioRBT.TabIndex = 9;
            this.DominioRBT.TabStop = true;
            this.DominioRBT.Text = "Domínio";
            this.DominioRBT.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(447, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 32);
            this.label9.TabIndex = 7;
            this.label9.Text = "COFRE";
            // 
            // FiltarFavoritosBT
            // 
            this.FiltarFavoritosBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltarFavoritosBT.Location = new System.Drawing.Point(10, 48);
            this.FiltarFavoritosBT.Name = "FiltarFavoritosBT";
            this.FiltarFavoritosBT.Size = new System.Drawing.Size(30, 25);
            this.FiltarFavoritosBT.TabIndex = 6;
            this.FiltarFavoritosBT.Text = "";
            this.Dica.SetToolTip(this.FiltarFavoritosBT, "Buscar em favoritos");
            this.FiltarFavoritosBT.UseVisualStyleBackColor = true;
            this.FiltarFavoritosBT.Click += new System.EventHandler(this.FiltarFavoritosBT_Click);
            // 
            // FiltragemTB
            // 
            this.FiltragemTB.Location = new System.Drawing.Point(46, 51);
            this.FiltragemTB.Name = "FiltragemTB";
            this.FiltragemTB.Size = new System.Drawing.Size(215, 20);
            this.FiltragemTB.TabIndex = 1;
            this.FiltragemTB.TextChanged += new System.EventHandler(this.FiltragemTB_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 386);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(684, 43);
            this.panel2.TabIndex = 1;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.LaunchImportingWizardBT);
            this.panel8.Controls.Add(this.SobreBT);
            this.panel8.Controls.Add(this.UsuarioBT);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(449, 41);
            this.panel8.TabIndex = 1;
            // 
            // LaunchImportingWizardBT
            // 
            this.LaunchImportingWizardBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LaunchImportingWizardBT.Location = new System.Drawing.Point(98, 8);
            this.LaunchImportingWizardBT.Name = "LaunchImportingWizardBT";
            this.LaunchImportingWizardBT.Size = new System.Drawing.Size(30, 30);
            this.LaunchImportingWizardBT.TabIndex = 2;
            this.LaunchImportingWizardBT.Text = "";
            this.Dica.SetToolTip(this.LaunchImportingWizardBT, "Iniciar assistente de importação de senhas");
            this.LaunchImportingWizardBT.UseVisualStyleBackColor = true;
            this.LaunchImportingWizardBT.Click += new System.EventHandler(this.LaunchImportingWizardBT_Click);
            // 
            // SobreBT
            // 
            this.SobreBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SobreBT.Location = new System.Drawing.Point(47, 8);
            this.SobreBT.Name = "SobreBT";
            this.SobreBT.Size = new System.Drawing.Size(30, 30);
            this.SobreBT.TabIndex = 2;
            this.SobreBT.Text = "";
            this.Dica.SetToolTip(this.SobreBT, "Sobre");
            this.SobreBT.UseVisualStyleBackColor = true;
            this.SobreBT.Click += new System.EventHandler(this.SobreBT_Click);
            // 
            // UsuarioBT
            // 
            this.UsuarioBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsuarioBT.Location = new System.Drawing.Point(11, 8);
            this.UsuarioBT.Name = "UsuarioBT";
            this.UsuarioBT.Size = new System.Drawing.Size(30, 30);
            this.UsuarioBT.TabIndex = 1;
            this.UsuarioBT.Text = "";
            this.Dica.SetToolTip(this.UsuarioBT, "Usuário");
            this.UsuarioBT.UseVisualStyleBackColor = true;
            this.UsuarioBT.Click += new System.EventHandler(this.UsuarioBT_Click);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(449, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(233, 41);
            this.panel7.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.VisualizadorContas);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 80);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(684, 306);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel4.Location = new System.Drawing.Point(387, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(297, 306);
            this.panel4.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.ChecarIntegridadeBT);
            this.panel6.Controls.Add(this.AssinaturaTB);
            this.panel6.Controls.Add(this.HashTB);
            this.panel6.Controls.Add(this.IntegridadeTB);
            this.panel6.Controls.Add(this.PassTB);
            this.panel6.Controls.Add(this.ModificadoEmTB);
            this.panel6.Controls.Add(this.CriadoEmTB);
            this.panel6.Controls.Add(this.UsuarioTB);
            this.panel6.Controls.Add(this.DominioTB);
            this.panel6.Controls.Add(this.RevelarSenhaBt);
            this.panel6.Controls.Add(this.CopiarSenhaBT);
            this.panel6.Controls.Add(this.AssinaturaLB);
            this.panel6.Controls.Add(this.HashLB);
            this.panel6.Controls.Add(this.IntegridadeLB);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.label1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(297, 264);
            this.panel6.TabIndex = 10;
            // 
            // ChecarIntegridadeBT
            // 
            this.ChecarIntegridadeBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChecarIntegridadeBT.Location = new System.Drawing.Point(240, 168);
            this.ChecarIntegridadeBT.Name = "ChecarIntegridadeBT";
            this.ChecarIntegridadeBT.Size = new System.Drawing.Size(30, 30);
            this.ChecarIntegridadeBT.TabIndex = 26;
            this.ChecarIntegridadeBT.Text = "";
            this.Dica.SetToolTip(this.ChecarIntegridadeBT, "Checar integridade");
            this.ChecarIntegridadeBT.UseVisualStyleBackColor = true;
            this.ChecarIntegridadeBT.Visible = false;
            this.ChecarIntegridadeBT.Click += new System.EventHandler(this.ChecarIntegridadeBT_Click);
            // 
            // AssinaturaTB
            // 
            this.AssinaturaTB.Location = new System.Drawing.Point(105, 228);
            this.AssinaturaTB.Name = "AssinaturaTB";
            this.AssinaturaTB.ReadOnly = true;
            this.AssinaturaTB.Size = new System.Drawing.Size(165, 23);
            this.AssinaturaTB.TabIndex = 25;
            this.AssinaturaTB.Visible = false;
            // 
            // HashTB
            // 
            this.HashTB.Location = new System.Drawing.Point(105, 198);
            this.HashTB.Name = "HashTB";
            this.HashTB.ReadOnly = true;
            this.HashTB.Size = new System.Drawing.Size(165, 23);
            this.HashTB.TabIndex = 24;
            this.HashTB.Visible = false;
            // 
            // IntegridadeTB
            // 
            this.IntegridadeTB.Location = new System.Drawing.Point(105, 168);
            this.IntegridadeTB.Name = "IntegridadeTB";
            this.IntegridadeTB.ReadOnly = true;
            this.IntegridadeTB.Size = new System.Drawing.Size(129, 23);
            this.IntegridadeTB.TabIndex = 23;
            this.IntegridadeTB.Visible = false;
            // 
            // PassTB
            // 
            this.PassTB.Location = new System.Drawing.Point(64, 138);
            this.PassTB.Name = "PassTB";
            this.PassTB.PasswordChar = '*';
            this.PassTB.ReadOnly = true;
            this.PassTB.Size = new System.Drawing.Size(134, 23);
            this.PassTB.TabIndex = 22;
            // 
            // ModificadoEmTB
            // 
            this.ModificadoEmTB.Location = new System.Drawing.Point(130, 107);
            this.ModificadoEmTB.Name = "ModificadoEmTB";
            this.ModificadoEmTB.ReadOnly = true;
            this.ModificadoEmTB.Size = new System.Drawing.Size(140, 23);
            this.ModificadoEmTB.TabIndex = 21;
            // 
            // CriadoEmTB
            // 
            this.CriadoEmTB.Location = new System.Drawing.Point(105, 78);
            this.CriadoEmTB.Name = "CriadoEmTB";
            this.CriadoEmTB.ReadOnly = true;
            this.CriadoEmTB.Size = new System.Drawing.Size(165, 23);
            this.CriadoEmTB.TabIndex = 20;
            // 
            // UsuarioTB
            // 
            this.UsuarioTB.Location = new System.Drawing.Point(82, 51);
            this.UsuarioTB.Name = "UsuarioTB";
            this.UsuarioTB.ReadOnly = true;
            this.UsuarioTB.Size = new System.Drawing.Size(188, 23);
            this.UsuarioTB.TabIndex = 19;
            // 
            // DominioTB
            // 
            this.DominioTB.Location = new System.Drawing.Point(82, 20);
            this.DominioTB.Name = "DominioTB";
            this.DominioTB.ReadOnly = true;
            this.DominioTB.Size = new System.Drawing.Size(188, 23);
            this.DominioTB.TabIndex = 7;
            // 
            // RevelarSenhaBt
            // 
            this.RevelarSenhaBt.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RevelarSenhaBt.Location = new System.Drawing.Point(240, 136);
            this.RevelarSenhaBt.Name = "RevelarSenhaBt";
            this.RevelarSenhaBt.Size = new System.Drawing.Size(30, 30);
            this.RevelarSenhaBt.TabIndex = 18;
            this.RevelarSenhaBt.Text = "";
            this.Dica.SetToolTip(this.RevelarSenhaBt, "Revelar senha");
            this.RevelarSenhaBt.UseVisualStyleBackColor = true;
            this.RevelarSenhaBt.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RevelarSenhaBt_MouseDown);
            this.RevelarSenhaBt.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RevelarSenhaBt_MouseUp);
            // 
            // CopiarSenhaBT
            // 
            this.CopiarSenhaBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CopiarSenhaBT.Location = new System.Drawing.Point(204, 136);
            this.CopiarSenhaBT.Name = "CopiarSenhaBT";
            this.CopiarSenhaBT.Size = new System.Drawing.Size(30, 30);
            this.CopiarSenhaBT.TabIndex = 4;
            this.CopiarSenhaBT.Text = "";
            this.Dica.SetToolTip(this.CopiarSenhaBT, "Copiar senha");
            this.CopiarSenhaBT.UseVisualStyleBackColor = true;
            this.CopiarSenhaBT.Click += new System.EventHandler(this.CopiarSenhaBT_Click);
            // 
            // AssinaturaLB
            // 
            this.AssinaturaLB.AutoSize = true;
            this.AssinaturaLB.Location = new System.Drawing.Point(9, 231);
            this.AssinaturaLB.Name = "AssinaturaLB";
            this.AssinaturaLB.Size = new System.Drawing.Size(93, 16);
            this.AssinaturaLB.TabIndex = 17;
            this.AssinaturaLB.Tag = "";
            this.AssinaturaLB.Text = "ASSINATURA";
            this.AssinaturaLB.Visible = false;
            // 
            // HashLB
            // 
            this.HashLB.AutoSize = true;
            this.HashLB.Location = new System.Drawing.Point(9, 201);
            this.HashLB.Name = "HashLB";
            this.HashLB.Size = new System.Drawing.Size(44, 16);
            this.HashLB.TabIndex = 16;
            this.HashLB.Tag = "";
            this.HashLB.Text = "HASH";
            this.HashLB.Visible = false;
            // 
            // IntegridadeLB
            // 
            this.IntegridadeLB.AutoSize = true;
            this.IntegridadeLB.Location = new System.Drawing.Point(9, 171);
            this.IntegridadeLB.Name = "IntegridadeLB";
            this.IntegridadeLB.Size = new System.Drawing.Size(96, 16);
            this.IntegridadeLB.TabIndex = 15;
            this.IntegridadeLB.Tag = "";
            this.IntegridadeLB.Text = "INTEGRIDADE";
            this.IntegridadeLB.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 13;
            this.label5.Tag = "";
            this.label5.Text = "SENHA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 16);
            this.label4.TabIndex = 12;
            this.label4.Tag = "";
            this.label4.Text = "MODIFICADO EM";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 16);
            this.label3.TabIndex = 11;
            this.label3.Tag = "";
            this.label3.Text = "CRIADO EM";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 16);
            this.label2.TabIndex = 10;
            this.label2.Tag = "";
            this.label2.Text = "USUÁRIO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 9;
            this.label1.Tag = "";
            this.label1.Text = "DOMÍNIO";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.FavoritarBT);
            this.panel5.Controls.Add(this.ShowAdvDataBT);
            this.panel5.Controls.Add(this.ExcluirContaBT);
            this.panel5.Controls.Add(this.ConfigSenhaBT);
            this.panel5.Controls.Add(this.NovaSenha);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 264);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(297, 42);
            this.panel5.TabIndex = 9;
            // 
            // FavoritarBT
            // 
            this.FavoritarBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FavoritarBT.Location = new System.Drawing.Point(77, 5);
            this.FavoritarBT.Name = "FavoritarBT";
            this.FavoritarBT.Size = new System.Drawing.Size(30, 30);
            this.FavoritarBT.TabIndex = 5;
            this.FavoritarBT.Text = "";
            this.Dica.SetToolTip(this.FavoritarBT, "Adicionar aos favoritos");
            this.FavoritarBT.UseVisualStyleBackColor = true;
            this.FavoritarBT.Click += new System.EventHandler(this.FavoritarBT_Click);
            // 
            // ShowAdvDataBT
            // 
            this.ShowAdvDataBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowAdvDataBT.ForeColor = System.Drawing.Color.Peru;
            this.ShowAdvDataBT.Location = new System.Drawing.Point(200, 5);
            this.ShowAdvDataBT.Name = "ShowAdvDataBT";
            this.ShowAdvDataBT.Size = new System.Drawing.Size(30, 30);
            this.ShowAdvDataBT.TabIndex = 4;
            this.ShowAdvDataBT.Text = "";
            this.Dica.SetToolTip(this.ShowAdvDataBT, "Avançado");
            this.ShowAdvDataBT.UseVisualStyleBackColor = true;
            this.ShowAdvDataBT.Click += new System.EventHandler(this.ShowAdvDataBT_Click);
            // 
            // ExcluirContaBT
            // 
            this.ExcluirContaBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExcluirContaBT.ForeColor = System.Drawing.Color.Red;
            this.ExcluirContaBT.Location = new System.Drawing.Point(236, 5);
            this.ExcluirContaBT.Name = "ExcluirContaBT";
            this.ExcluirContaBT.Size = new System.Drawing.Size(30, 30);
            this.ExcluirContaBT.TabIndex = 3;
            this.ExcluirContaBT.Text = "";
            this.Dica.SetToolTip(this.ExcluirContaBT, "Remover senha");
            this.ExcluirContaBT.UseVisualStyleBackColor = true;
            this.ExcluirContaBT.Click += new System.EventHandler(this.ExcluirContaBT_Click);
            // 
            // ConfigSenhaBT
            // 
            this.ConfigSenhaBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfigSenhaBT.Location = new System.Drawing.Point(41, 5);
            this.ConfigSenhaBT.Name = "ConfigSenhaBT";
            this.ConfigSenhaBT.Size = new System.Drawing.Size(30, 30);
            this.ConfigSenhaBT.TabIndex = 1;
            this.ConfigSenhaBT.Text = "";
            this.Dica.SetToolTip(this.ConfigSenhaBT, "Configurações de senha");
            this.ConfigSenhaBT.UseVisualStyleBackColor = true;
            this.ConfigSenhaBT.Click += new System.EventHandler(this.ConfigSenhaBT_Click);
            // 
            // NovaSenha
            // 
            this.NovaSenha.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NovaSenha.Location = new System.Drawing.Point(5, 5);
            this.NovaSenha.Name = "NovaSenha";
            this.NovaSenha.Size = new System.Drawing.Size(30, 30);
            this.NovaSenha.TabIndex = 0;
            this.NovaSenha.Text = "";
            this.Dica.SetToolTip(this.NovaSenha, "Nova senha");
            this.NovaSenha.UseVisualStyleBackColor = true;
            this.NovaSenha.Click += new System.EventHandler(this.NovaSenha_Click);
            // 
            // VisualizadorContas
            // 
            this.VisualizadorContas.AllowUserToAddRows = false;
            this.VisualizadorContas.AllowUserToDeleteRows = false;
            this.VisualizadorContas.AutoGenerateColumns = false;
            this.VisualizadorContas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.VisualizadorContas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.favoritoDataGridViewCheckBoxColumn,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.obsDataGridViewTextBoxColumn});
            this.VisualizadorContas.DataSource = this.contaBindingSource;
            this.VisualizadorContas.Dock = System.Windows.Forms.DockStyle.Left;
            this.VisualizadorContas.Location = new System.Drawing.Point(0, 0);
            this.VisualizadorContas.MultiSelect = false;
            this.VisualizadorContas.Name = "VisualizadorContas";
            this.VisualizadorContas.ReadOnly = true;
            this.VisualizadorContas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.VisualizadorContas.Size = new System.Drawing.Size(387, 306);
            this.VisualizadorContas.TabIndex = 2;
            this.VisualizadorContas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.VisualizadorContas_CellClick);
            this.VisualizadorContas.SelectionChanged += new System.EventHandler(this.VisualizadorContas_SelectionChanged);
            this.VisualizadorContas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.VisualizadorContas_MouseUp);
            // 
            // favoritoDataGridViewCheckBoxColumn
            // 
            this.favoritoDataGridViewCheckBoxColumn.DataPropertyName = "Favorito";
            this.favoritoDataGridViewCheckBoxColumn.HeaderText = "Favorito";
            this.favoritoDataGridViewCheckBoxColumn.Name = "favoritoDataGridViewCheckBoxColumn";
            this.favoritoDataGridViewCheckBoxColumn.ReadOnly = true;
            this.favoritoDataGridViewCheckBoxColumn.Width = 35;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Dominio";
            this.dataGridViewTextBoxColumn1.HeaderText = "Dominio";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Usuario";
            this.dataGridViewTextBoxColumn2.HeaderText = "Usuario";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // obsDataGridViewTextBoxColumn
            // 
            this.obsDataGridViewTextBoxColumn.DataPropertyName = "Obs";
            this.obsDataGridViewTextBoxColumn.HeaderText = "Observação";
            this.obsDataGridViewTextBoxColumn.Name = "obsDataGridViewTextBoxColumn";
            this.obsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // contaBindingSource
            // 
            this.contaBindingSource.DataSource = typeof(Valut.Accounts.Conta);
            this.contaBindingSource.Filter = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 429);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(700, 468);
            this.MinimumSize = new System.Drawing.Size(700, 468);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Valut";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VisualizadorContas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.contaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button RevelarSenhaBt;
        private System.Windows.Forms.Button CopiarSenhaBT;
        private System.Windows.Forms.Label AssinaturaLB;
        private System.Windows.Forms.Label HashLB;
        private System.Windows.Forms.Label IntegridadeLB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button ShowAdvDataBT;
        private System.Windows.Forms.Button ExcluirContaBT;
        private System.Windows.Forms.Button ConfigSenhaBT;
        private System.Windows.Forms.Button NovaSenha;
        private System.Windows.Forms.ToolTip Dica;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button FiltarFavoritosBT;
        private System.Windows.Forms.TextBox FiltragemTB;
        private System.Windows.Forms.Button SobreBT;
        private System.Windows.Forms.Button UsuarioBT;
        private System.Windows.Forms.TextBox AssinaturaTB;
        private System.Windows.Forms.TextBox HashTB;
        private System.Windows.Forms.TextBox IntegridadeTB;
        private System.Windows.Forms.TextBox PassTB;
        private System.Windows.Forms.TextBox ModificadoEmTB;
        private System.Windows.Forms.TextBox CriadoEmTB;
        private System.Windows.Forms.TextBox UsuarioTB;
        private System.Windows.Forms.TextBox DominioTB;
        private System.Windows.Forms.Button FavoritarBT;
        private System.Windows.Forms.RadioButton DominioRBT;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton UsuarioRBT;
        private System.Windows.Forms.Button ChecarIntegridadeBT;
        private System.Windows.Forms.DataGridView VisualizadorContas;
        private System.Windows.Forms.DataGridViewTextBoxColumn dominioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn usuarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource contaBindingSource;
        private System.Windows.Forms.DataGridViewCheckBoxColumn favoritoDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn obsDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button LaunchImportingWizardBT;
    }
}

