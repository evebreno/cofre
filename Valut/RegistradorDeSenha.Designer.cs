﻿namespace Valut
{
    partial class RegistradorDeSenha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistradorDeSenha));
            this.DominioText = new System.Windows.Forms.TextBox();
            this.UsuarioText = new System.Windows.Forms.TextBox();
            this.SenhaText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SalvarDados = new System.Windows.Forms.Button();
            this.LimparCampo = new System.Windows.Forms.Button();
            this.ExibirSenha = new System.Windows.Forms.Button();
            this.ObsTB = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.WizardBT = new System.Windows.Forms.Button();
            this.GeradorMP = new System.Windows.Forms.Panel();
            this.GeneratedPassTB = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SymbolCB = new System.Windows.Forms.CheckBox();
            this.NumberCB = new System.Windows.Forms.CheckBox();
            this.LowerCB = new System.Windows.Forms.CheckBox();
            this.UpperCB = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.MaxNUD = new System.Windows.Forms.NumericUpDown();
            this.MinNumUD = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.GeneratePassBT = new System.Windows.Forms.Button();
            this.SavePassBT = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.GeradorMP.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinNumUD)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // DominioText
            // 
            this.DominioText.Location = new System.Drawing.Point(120, 34);
            this.DominioText.Name = "DominioText";
            this.DominioText.Size = new System.Drawing.Size(135, 20);
            this.DominioText.TabIndex = 0;
            // 
            // UsuarioText
            // 
            this.UsuarioText.Location = new System.Drawing.Point(120, 71);
            this.UsuarioText.Name = "UsuarioText";
            this.UsuarioText.Size = new System.Drawing.Size(135, 20);
            this.UsuarioText.TabIndex = 1;
            // 
            // SenhaText
            // 
            this.SenhaText.Location = new System.Drawing.Point(120, 107);
            this.SenhaText.Name = "SenhaText";
            this.SenhaText.PasswordChar = '*';
            this.SenhaText.Size = new System.Drawing.Size(135, 20);
            this.SenhaText.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Domínio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 24);
            this.label2.TabIndex = 3;
            this.label2.Text = "Usuário";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "Senha";
            // 
            // SalvarDados
            // 
            this.SalvarDados.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SalvarDados.Location = new System.Drawing.Point(306, 240);
            this.SalvarDados.Name = "SalvarDados";
            this.SalvarDados.Size = new System.Drawing.Size(34, 36);
            this.SalvarDados.TabIndex = 4;
            this.SalvarDados.Text = "";
            this.toolTip1.SetToolTip(this.SalvarDados, "Salvar senha");
            this.SalvarDados.UseVisualStyleBackColor = true;
            this.SalvarDados.Click += new System.EventHandler(this.SalvarDados_Click);
            // 
            // LimparCampo
            // 
            this.LimparCampo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.LimparCampo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LimparCampo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LimparCampo.Location = new System.Drawing.Point(264, 240);
            this.LimparCampo.Name = "LimparCampo";
            this.LimparCampo.Size = new System.Drawing.Size(36, 36);
            this.LimparCampo.TabIndex = 5;
            this.LimparCampo.Text = "";
            this.toolTip1.SetToolTip(this.LimparCampo, "Cancelar");
            this.LimparCampo.UseVisualStyleBackColor = true;
            this.LimparCampo.Click += new System.EventHandler(this.LimparCampo_Click);
            // 
            // ExibirSenha
            // 
            this.ExibirSenha.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExibirSenha.Location = new System.Drawing.Point(261, 106);
            this.ExibirSenha.Name = "ExibirSenha";
            this.ExibirSenha.Size = new System.Drawing.Size(30, 22);
            this.ExibirSenha.TabIndex = 19;
            this.ExibirSenha.Text = "";
            this.toolTip1.SetToolTip(this.ExibirSenha, "Mostrar senha");
            this.ExibirSenha.UseVisualStyleBackColor = true;
            this.ExibirSenha.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Revelar);
            this.ExibirSenha.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Ocultar);
            // 
            // ObsTB
            // 
            this.ObsTB.Location = new System.Drawing.Point(38, 161);
            this.ObsTB.Name = "ObsTB";
            this.ObsTB.Size = new System.Drawing.Size(302, 78);
            this.ObsTB.TabIndex = 3;
            this.ObsTB.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(35, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Observações";
            // 
            // WizardBT
            // 
            this.WizardBT.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WizardBT.Location = new System.Drawing.Point(297, 107);
            this.WizardBT.Name = "WizardBT";
            this.WizardBT.Size = new System.Drawing.Size(30, 22);
            this.WizardBT.TabIndex = 19;
            this.WizardBT.Text = "";
            this.toolTip1.SetToolTip(this.WizardBT, "Gerador de senha");
            this.WizardBT.UseVisualStyleBackColor = true;
            this.WizardBT.Click += new System.EventHandler(this.WizardBT_Click);
            // 
            // GeradorMP
            // 
            this.GeradorMP.Controls.Add(this.GeneratedPassTB);
            this.GeradorMP.Controls.Add(this.groupBox1);
            this.GeradorMP.Controls.Add(this.panel1);
            this.GeradorMP.Controls.Add(this.panel2);
            this.GeradorMP.Controls.Add(this.GeneratePassBT);
            this.GeradorMP.Controls.Add(this.SavePassBT);
            this.GeradorMP.Dock = System.Windows.Forms.DockStyle.Right;
            this.GeradorMP.Location = new System.Drawing.Point(45, 0);
            this.GeradorMP.Name = "GeradorMP";
            this.GeradorMP.Size = new System.Drawing.Size(289, 288);
            this.GeradorMP.TabIndex = 21;
            this.GeradorMP.Visible = false;
            // 
            // GeneratedPassTB
            // 
            this.GeneratedPassTB.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GeneratedPassTB.Location = new System.Drawing.Point(12, 252);
            this.GeneratedPassTB.Name = "GeneratedPassTB";
            this.GeneratedPassTB.ReadOnly = true;
            this.GeneratedPassTB.Size = new System.Drawing.Size(223, 22);
            this.GeneratedPassTB.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SymbolCB);
            this.groupBox1.Controls.Add(this.NumberCB);
            this.groupBox1.Controls.Add(this.LowerCB);
            this.groupBox1.Controls.Add(this.UpperCB);
            this.groupBox1.Location = new System.Drawing.Point(4, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(153, 114);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deve conter";
            // 
            // SymbolCB
            // 
            this.SymbolCB.AutoSize = true;
            this.SymbolCB.Location = new System.Drawing.Point(8, 86);
            this.SymbolCB.Name = "SymbolCB";
            this.SymbolCB.Size = new System.Drawing.Size(113, 17);
            this.SymbolCB.TabIndex = 0;
            this.SymbolCB.Text = "Especiais (#@$...)";
            this.SymbolCB.UseVisualStyleBackColor = true;
            // 
            // NumberCB
            // 
            this.NumberCB.AutoSize = true;
            this.NumberCB.Location = new System.Drawing.Point(8, 63);
            this.NumberCB.Name = "NumberCB";
            this.NumberCB.Size = new System.Drawing.Size(68, 17);
            this.NumberCB.TabIndex = 0;
            this.NumberCB.Text = "Números";
            this.NumberCB.UseVisualStyleBackColor = true;
            // 
            // LowerCB
            // 
            this.LowerCB.AutoSize = true;
            this.LowerCB.Checked = true;
            this.LowerCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.LowerCB.Location = new System.Drawing.Point(8, 43);
            this.LowerCB.Name = "LowerCB";
            this.LowerCB.Size = new System.Drawing.Size(79, 17);
            this.LowerCB.TabIndex = 0;
            this.LowerCB.Text = "Minúsculas";
            this.LowerCB.UseVisualStyleBackColor = true;
            // 
            // UpperCB
            // 
            this.UpperCB.AutoSize = true;
            this.UpperCB.Checked = true;
            this.UpperCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.UpperCB.Location = new System.Drawing.Point(8, 20);
            this.UpperCB.Name = "UpperCB";
            this.UpperCB.Size = new System.Drawing.Size(79, 17);
            this.UpperCB.TabIndex = 0;
            this.UpperCB.Text = "Maiúsculas";
            this.UpperCB.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.MaxNUD);
            this.panel1.Controls.Add(this.MinNumUD);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(4, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(153, 82);
            this.panel1.TabIndex = 1;
            // 
            // MaxNUD
            // 
            this.MaxNUD.Location = new System.Drawing.Point(48, 48);
            this.MaxNUD.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.MaxNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MaxNUD.Name = "MaxNUD";
            this.MaxNUD.Size = new System.Drawing.Size(64, 20);
            this.MaxNUD.TabIndex = 1;
            this.MaxNUD.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.MaxNUD.ValueChanged += new System.EventHandler(this.MaxNUD_ValueChanged);
            // 
            // MinNumUD
            // 
            this.MinNumUD.Location = new System.Drawing.Point(48, 22);
            this.MinNumUD.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.MinNumUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.MinNumUD.Name = "MinNumUD";
            this.MinNumUD.Size = new System.Drawing.Size(64, 20);
            this.MinNumUD.TabIndex = 1;
            this.MinNumUD.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.MinNumUD.ValueChanged += new System.EventHandler(this.MinNumUD_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Máximo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Minímo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Quantidade de caracteres";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label5);
            this.panel2.Location = new System.Drawing.Point(4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(279, 33);
            this.panel2.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(69, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(149, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Gerador de senha V 1.0";
            // 
            // GeneratePassBT
            // 
            this.GeneratePassBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GeneratePassBT.Location = new System.Drawing.Point(172, 195);
            this.GeneratePassBT.Name = "GeneratePassBT";
            this.GeneratePassBT.Size = new System.Drawing.Size(96, 36);
            this.GeneratePassBT.TabIndex = 4;
            this.GeneratePassBT.Text = "Gerar senha";
            this.GeneratePassBT.UseVisualStyleBackColor = true;
            this.GeneratePassBT.Click += new System.EventHandler(this.GeneratePassBT_Click);
            // 
            // SavePassBT
            // 
            this.SavePassBT.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavePassBT.Location = new System.Drawing.Point(243, 240);
            this.SavePassBT.Name = "SavePassBT";
            this.SavePassBT.Size = new System.Drawing.Size(34, 36);
            this.SavePassBT.TabIndex = 4;
            this.SavePassBT.Text = "";
            this.toolTip1.SetToolTip(this.SavePassBT, "Enviar para o campo senha");
            this.SavePassBT.UseVisualStyleBackColor = true;
            this.SavePassBT.Click += new System.EventHandler(this.SavePassBT_Click);
            // 
            // RegistradorDeSenha
            // 
            this.AcceptButton = this.SalvarDados;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.LimparCampo;
            this.ClientSize = new System.Drawing.Size(334, 288);
            this.Controls.Add(this.GeradorMP);
            this.Controls.Add(this.ObsTB);
            this.Controls.Add(this.WizardBT);
            this.Controls.Add(this.ExibirSenha);
            this.Controls.Add(this.LimparCampo);
            this.Controls.Add(this.SalvarDados);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SenhaText);
            this.Controls.Add(this.UsuarioText);
            this.Controls.Add(this.DominioText);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegistradorDeSenha";
            this.Text = "Editor de conta";
            this.GeradorMP.ResumeLayout(false);
            this.GeradorMP.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MinNumUD)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox DominioText;
        private System.Windows.Forms.TextBox UsuarioText;
        private System.Windows.Forms.TextBox SenhaText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SalvarDados;
        private System.Windows.Forms.Button LimparCampo;
        private System.Windows.Forms.Button ExibirSenha;
        private System.Windows.Forms.RichTextBox ObsTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button WizardBT;
        private System.Windows.Forms.Panel GeradorMP;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown MaxNUD;
        private System.Windows.Forms.NumericUpDown MinNumUD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox GeneratedPassTB;
        private System.Windows.Forms.Button GeneratePassBT;
        private System.Windows.Forms.Button SavePassBT;
        private System.Windows.Forms.CheckBox SymbolCB;
        private System.Windows.Forms.CheckBox NumberCB;
        private System.Windows.Forms.CheckBox LowerCB;
        private System.Windows.Forms.CheckBox UpperCB;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}