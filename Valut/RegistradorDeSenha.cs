﻿using System;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using Valut.FileManager;
using Valut.Encrypting;
using Valut.Accounts;

namespace Valut
{
    public partial class RegistradorDeSenha : Form
    {
        Random randomNumb = new Random();
        bool novaConta;
        Conta contaIndex;
        int originalWidth = 350;

        public RegistradorDeSenha()
        {
            InitializeComponent();
            novaConta = true;
        }

        //public RegistradorDeSenha(int conta)
        //    InitializeComponent();
        //    novaConta = false;
        //    contaIndex = conta;
        //    DominioText.Text = UserCenterData.ExpressiveData[conta].Dominio;
        //    UsuarioText.Text = UserCenterData.ExpressiveData[conta].Usuario;

        //    UserCenterData.ExpressiveData[conta].DecryptPassword(PerfilThread.GetPK2(UserCenterData.GetPass(true)).PrivateKey());
        //    SenhaText.Text = UserCenterData.ExpressiveData[conta].Senha.ShowString();
        //    UserCenterData.ExpressiveData[conta].EncryptPassword(PerfilThread.GetPK1(UserCenterData.GetPass(true)));
        //    ObsTB.Text = UserCenterData.ExpressiveData[conta].Obs;
        //
        ///}

        public  RegistradorDeSenha(Conta conta)
        {
            InitializeComponent();
            novaConta = false;

            DominioText.Text = conta.Dominio;
            UsuarioText.Text = conta.Usuario;
            contaIndex = conta;

            try
            {
                conta.DecryptPassword(PerfilThread.GetPK2(UserCenterData.GetPass(true)).PrivateKey());
                SenhaText.Text = conta.Senha.ShowString();
                conta.EncryptPassword(PerfilThread.GetPK1(UserCenterData.GetPass(true)));
                ObsTB.Text = conta.Obs;
            }
            catch(Exception e) { throw e; }
        }

        private void LimparCampo_Click(object sender, EventArgs e)
        {
            DominioText.Clear();
            UsuarioText.Clear();
            SenhaText.Clear();
        }

        private void Revelar(object sender, MouseEventArgs e)
        {
            SenhaText.PasswordChar = '\0';
        }

        private void Ocultar(object sender, MouseEventArgs e)
        {
            SenhaText.PasswordChar = '*';
        }

        private void SalvarDados_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(DominioText.Text) || string.IsNullOrWhiteSpace(UsuarioText.Text) || string.IsNullOrWhiteSpace(SenhaText.Text))
                {
                    MessageBox.Show("Algum campo obrigatório está em branco.\nPara prosseguir é necessário preencher o formulário corretamente.");
                }

                else
                {
                    if (novaConta)
                    {
                        try
                        {
                            UserCenterData.GetPass(true);
                            Conta conta = new Conta(DominioText.Text, UsuarioText.Text, SenhaText.Text, ObsTB.Text);
                            UserCenterData.AddAccount(conta, true);
                            this.Close();
                        }

                        catch (EncryptingExceptions.IncorrrectPasswordException)
                        { UserCenterData.AbortPass(); MessageBox.Show("Senha incorreta.");}

                        catch(EncryptingExceptions.PasswordNotSetException)
                        { UserCenterData.AbortPass(); MessageBox.Show("Digite a senha."); }

                        catch(Exception ex)
                        { MessageBox.Show($"Erro inesperado: {ex.Message}"); this.Close(); }
                        
                    }

                    else
                    {
                        try
                        {
                            UserCenterData.GetPass(true);
                            contaIndex.Dominio = DominioText.Text;
                            contaIndex.Usuario = UsuarioText.Text;
                            contaIndex.Obs = ObsTB.Text;
                            contaIndex.ModificadoEm = DateTime.Today;
                            contaIndex.Senha = SenhaText.Text.HideString();
                            contaIndex.EncryptPassword(PerfilThread.GetPK1(UserCenterData.GetPass(false)));

                            UserCenterData.SyncData();
                            this.Close();
                        }

                        catch (EncryptingExceptions.IncorrrectPasswordException)
                        { UserCenterData.AbortPass(); MessageBox.Show("Senha incorreta."); }

                        catch (EncryptingExceptions.PasswordNotSetException)
                        { UserCenterData.AbortPass(); MessageBox.Show("Digite a senha."); }

                        catch (Exception ex)
                        { MessageBox.Show($"Erro inesperado: {ex.Message}"); this.Close(); }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Senha incorreta!");
            }
        }

        private void MinNumUD_ValueChanged(object sender, EventArgs e)
        {
            MaxNUD.Minimum = MinNumUD.Value;
        }

        private void MaxNUD_ValueChanged(object sender, EventArgs e)
        {
            MinNumUD.Maximum = MaxNUD.Value;
        }

        private void GeneratePassBT_Click(object sender, EventArgs e)
        {
            if (UpperCB.Checked || LowerCB.Checked || SymbolCB.Checked || NumberCB.Checked)
            {
                byte[] buff = new byte[randomNumb.Next((int)MinNumUD.Value, (int)MaxNUD.Value)];
                randomNumb.NextBytes(buff);
                char[] characters = new char[buff.Length];


                for (int i = 0; i < characters.Length; i++)
                { characters[i] = (char)buff[i]; }


                for (int i = 0; i < characters.Length; i++)
                {
                    bool accept = false;
                    while (!accept)
                    {
                        if (char.IsUpper(characters[i]) && UpperCB.Checked)
                        { accept = true; break; }

                        if (char.IsLower(characters[i]) && LowerCB.Checked)
                        { accept = true; break; }

                        if (char.IsDigit(characters[i]) && NumberCB.Checked)
                        { accept = true; break; }

                        if ((char.IsPunctuation(characters[i]) || char.IsSymbol(characters[i])) && SymbolCB.Checked && NormalizedSymbol(characters[i]))
                        { accept = true; break; }

                        byte[] tmpBuff = new byte[2];
                        randomNumb.NextBytes(tmpBuff);

                        characters[i] = (char)tmpBuff[0];
                    }
                }

                string pass = "";
                foreach(var b in characters)
                { pass += b; }

                GeneratedPassTB.Text = RemoverAcentos(pass);
            }
            else { MessageBox.Show("É necessário selecionar pelo menos umas das opções de caracteres");}
        }

        bool expanded = false;
        private void WizardBT_Click(object sender, EventArgs e)
        {
            if (expanded)
            {
                GeradorMP.Visible = false;
                this.Width = originalWidth;
                expanded = false;
            }

            else
            {
                this.Width = 655;
                GeradorMP.Visible = true;
                expanded = true;
            }
        }

        private void SavePassBT_Click(object sender, EventArgs e)
        {
            SenhaText.Text = GeneratedPassTB.Text;
        }

        private bool NormalizedSymbol(char text)
        {
            UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(text);
            return (uc == UnicodeCategory.MathSymbol) || (uc == UnicodeCategory.CurrencySymbol);
        }

        private string RemoverAcentos(string texto)
        {
            string s = texto.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for(int i = 0;i<s.Length;i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(s[i]);
                if(uc != UnicodeCategory.NonSpacingMark && uc != UnicodeCategory.OtherLetter && uc != UnicodeCategory.OtherNotAssigned && uc != UnicodeCategory.OtherSymbol)
                { sb.Append(s[i]); }
            }
            return sb.ToString();
        }
    }
}
