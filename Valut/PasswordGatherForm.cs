﻿using System;
using System.Windows.Forms;
using System.Security;
using Valut.Encrypting;

namespace Valut
{
    public partial class PasswordGatherForm : Form
    {
        public PasswordGatherForm()
        {
            InitializeComponent();
        }

        public SecureString GatherPassword()
        {
            if (this.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(PassBox.Text))
            {
                return PassBox.Text.HideString();
            }
            else return new SecureString();
        }

        private void CommitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void Cancelbutton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
