﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Valut
{
    static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            PrepareSystem();
            //CorruptFile();
        }

        static void PrepareSystem()
        {
            Form1 principal = new Form1();
            UserCenterData.InitializeData();

            try
            {
                Application.Run(principal);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocorreu um erro inseperado: {ex.Message}\n" +
                  $"Codigo: {ex.HResult}\nStackTrace: {ex.StackTrace}" +
                  $"\nSource{ex.Source}","Ops...");

                MessageBox.Show("O programa irá encerrar agora, caso o problema " +
                    "persista, favor entrar em contato com o programador do sistema.");

                Environment.Exit(0);

            }
        }
    }
}
