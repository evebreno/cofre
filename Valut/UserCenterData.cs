﻿using System;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Security;
using System.ComponentModel;
using Valut.Accounts;
using Valut.FileManager;
using Valut.Encrypting;
using System.Threading;
using System.Windows.Forms;

namespace Valut
{
    /// <summary>
    /// Contém as informações de usuário e senha em tempo de execução
    /// </summary>
    static class UserCenterData
    {
        static private SecureString MPass = new SecureString();
        static public BindingList<Conta> ExpressiveData = new BindingList<Conta>();   
        static public string MainFile { get { return $"{Directory.GetCurrentDirectory()}/.cofre"; } }
        static public EventHandler SyncingFile;
        static public EventHandler FileSyncronized;

        static internal Perfil PerfilData { get; set; }
        static private RSAParameters PK1 { get; set; }
        static public Diretorio Diretorio { get; }
        static private System.Threading.Timer Counter = new System.Threading.Timer(new TimerCallback(ClearPass),null,3000,3000);
        static public int PassTimeCount = 300000;

        static public void InitializeData()
        {
            FileStream file = null;
            AbortPass();
            try
            {
                file = new FileStream($"{Directory.GetCurrentDirectory()}/.cofre", FileMode.Open);
                MPass = GetPass(file, true);
                if (!Diretorio.CompareDirectory(file, Diretorio.GetCurrentDirectoryInfo()))
                { throw new EncryptingExceptions.IncorrrectPasswordException("Diretório não verificado!"); }

                PK1 = PerfilThread.GetPK1(file, MPass);
                RSAContainerData tmpContainer = PerfilThread.GetPK2(file, MPass);
                foreach (var conta in AccountsThread.ObterContas(file, tmpContainer.PrivateKey())) ExpressiveData.Add(conta);

                file.Close();
                PerfilData = FileCenter.RetrievePerfil(MPass);

                PassTimeCount = PerfilData.PasswordExpirationTime * 60000;
                Counter.Change(PassTimeCount, PassTimeCount);
            }

            catch (FileNotFoundException)
            {
                try
                {
                    UserGather ug = new UserGather();
                    string usuario = ug.GatherPerfilData(out SecureString senha);

                    MPass = senha.Copy();
                    FileCenter.CriarPerfil(usuario, senha.Copy());
                    PerfilData = FileCenter.RetrievePerfil(senha.Copy()); /*GetPass()*/

                    PassTimeCount = PerfilData.PasswordExpirationTime * 60000;
                    Counter.Change(PassTimeCount, PassTimeCount);
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"{ex.Message}");
                    Environment.Exit(0);
                }
            }

            catch (EncryptingExceptions.IncorrrectPasswordException)
            {
                try
                {
                    if (!Diretorio.CompareDirectory(file, Diretorio.GetCurrentDirectoryInfo()))
                    {
                        MessageBox.Show("O caminho do cofre a ser acessado não confere com o " +
                            "caminho onde ele foi criado.\nPara importar contas de um diretório " +
                            "para outro, use o assistente de importação de contas.", "Fraude detectada!");

                        Environment.Exit(0);
                    }

                    else
                    {
                        MessageBox.Show("Chave digitada está incorreta!\nTente novamente");
                        file.Close();
                        InitializeData();
                    }
                }
                catch
                {
                    TryRecover();
                }
            }

            catch (EncryptingExceptions.PasswordNotSetException)
            {
                Environment.Exit(0);
            }

            catch (Exception)
            {
                TryRecover();
            }

            void TryRecover()
            {
                if (MessageBox.Show("O cofre parece estar parcialmente corrompido ou " +
                    "danificado, mas ainda é possível recuperar parte de suas " +
                    "informações.\nDeseja tentar recuperá-lo?", "Cofre corrompido",
                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    try
                    {
                        FileRecoverWizard.RecoverFile(file, MPass);
                        file.Close();
                        SyncData();
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show($"Ococrreu um erro {ex.Message}");
                    }
                }
                else
                {
                    MessageBox.Show("O cofre será encerrado.");
                    Environment.Exit(0);
                }
            }
        }

        private static void ClearPass(object target)
        {
            MPass.Clear();
        }

        static public Perfil GetProfileData()
        {
            try
            {
                return FileCenter.RetrievePerfil(GetPass(true));
            }
            catch (Exception e) { throw e; }
        }

        static public void AddAccount (Conta conta, bool saveData)
        {
            ExpressiveData.Add(conta);
            
            if (saveData)
            {
                Thread tr = new Thread(SaveData);
                tr.Start();
            }
        }

        static public void SyncData()
        {
            Thread tr = new Thread(SaveData);
            tr.Start();
        }

        static private void SaveData()
        {
            CallSyncingFileMessage();
            FileCenter.UpdateFile(PerfilData, ExpressiveData.ToArray(), GetPass(true));
            CallSyncronizedFileMessage();
        }

        static internal void SaveData(SecureString password)
        {
            FileCenter.UpdateFile(PerfilData, ExpressiveData.ToArray(), password );
        }

        static public SecureString GetPass(bool verifyPass)
        {
            Counter.Change(PassTimeCount, PassTimeCount);
            if (MPass.Length != 0)
            {
                return MPass.Copy();
            }

            else
            {
                PasswordGatherForm pgf = new PasswordGatherForm();
                MPass = pgf.GatherPassword();
                if (MPass.Length != 0)
                {
                    if (verifyPass)
                    {
                        if (PerfilThread.ComparePass(MPass))
                            return MPass.Copy();

                        else
                        {
                            AbortPass();
                            throw new EncryptingExceptions.IncorrrectPasswordException("Senha incorreta!");
                        }
                    }

                    else
                    {
                        return MPass.Copy();
                    }

                }
                else
                {
                    AbortPass();
                    throw new EncryptingExceptions.PasswordNotSetException("O usuário não digitou a senha");
                }
            }
        }

        static public SecureString GetPass(FileStream file, bool verifyPass)
        {
            Counter.Change(PassTimeCount, PassTimeCount);
            if (MPass.Length != 0)
            {
                return MPass.Copy();
            }

            else
            {
                PasswordGatherForm pgf = new PasswordGatherForm();
                MPass = pgf.GatherPassword();
                if (MPass.Length != 0)
                {
                    if (verifyPass)
                    {
                        if (PerfilThread.ComparePass(file, MPass))
                            return MPass.Copy();

                        else throw new EncryptingExceptions.IncorrrectPasswordException("Senha incorreta!");
                    }

                    else
                    {
                        return MPass.Copy();
                    }

                }
                else throw new EncryptingExceptions.PasswordNotSetException("O usuário não digitou a senha");
            }
        }

        static public void AbortPass()
        {
            MPass.Clear();
            Counter.Change(0, PassTimeCount);
        }

        static private void CallSyncingFileMessage()
        {
            if(SyncingFile != null) { SyncingFile.Invoke(null, null); }
        }

        static private void CallSyncronizedFileMessage()
        {
            if(FileSyncronized != null) { FileSyncronized.Invoke(null, null); }
        }

        static public void ChangeCounter(int timeInMinutes)
        {
            int time = timeInMinutes * 60000;
            PassTimeCount = time;
            Counter.Change(time, time);
        }
    }
}
