﻿namespace Valut
{
    partial class UsuarioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsuarioForm));
            this.UserNameTB = new System.Windows.Forms.TextBox();
            this.TimeTkB = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.CommitNewPassBt = new System.Windows.Forms.Button();
            this.MinuteLB = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CommitPassTB = new System.Windows.Forms.TextBox();
            this.NewPassTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.CreationAccTB = new System.Windows.Forms.TextBox();
            this.LastModifTB = new System.Windows.Forms.TextBox();
            this.SaveChangesBT = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TimeTkB)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserNameTB
            // 
            this.UserNameTB.Location = new System.Drawing.Point(13, 13);
            this.UserNameTB.Name = "UserNameTB";
            this.UserNameTB.Size = new System.Drawing.Size(194, 20);
            this.UserNameTB.TabIndex = 0;
            // 
            // TimeTkB
            // 
            this.TimeTkB.Location = new System.Drawing.Point(12, 65);
            this.TimeTkB.Maximum = 6;
            this.TimeTkB.Minimum = 1;
            this.TimeTkB.Name = "TimeTkB";
            this.TimeTkB.Size = new System.Drawing.Size(104, 45);
            this.TimeTkB.TabIndex = 1;
            this.toolTip1.SetToolTip(this.TimeTkB, "Quanto menor, mais seguro");
            this.TimeTkB.Value = 5;
            this.TimeTkB.Scroll += new System.EventHandler(this.TimeTkB_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tempo de armazenagem de senha";
            // 
            // CommitNewPassBt
            // 
            this.CommitNewPassBt.Location = new System.Drawing.Point(119, 71);
            this.CommitNewPassBt.Name = "CommitNewPassBt";
            this.CommitNewPassBt.Size = new System.Drawing.Size(75, 23);
            this.CommitNewPassBt.TabIndex = 4;
            this.CommitNewPassBt.Text = "Confirmar";
            this.toolTip1.SetToolTip(this.CommitNewPassBt, "Alterar senha (este botão é independente de \"Salvar mudanças\"");
            this.CommitNewPassBt.UseVisualStyleBackColor = true;
            this.CommitNewPassBt.Click += new System.EventHandler(this.CommitNewPassBt_Click);
            // 
            // MinuteLB
            // 
            this.MinuteLB.AutoSize = true;
            this.MinuteLB.Location = new System.Drawing.Point(123, 65);
            this.MinuteLB.Name = "MinuteLB";
            this.MinuteLB.Size = new System.Drawing.Size(53, 13);
            this.MinuteLB.TabIndex = 2;
            this.MinuteLB.Text = "0 Minutos";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CommitNewPassBt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.CommitPassTB);
            this.groupBox1.Controls.Add(this.NewPassTB);
            this.groupBox1.Location = new System.Drawing.Point(13, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 108);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alterar senha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Confirmar:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nova:";
            // 
            // CommitPassTB
            // 
            this.CommitPassTB.Location = new System.Drawing.Point(71, 45);
            this.CommitPassTB.Name = "CommitPassTB";
            this.CommitPassTB.PasswordChar = '*';
            this.CommitPassTB.Size = new System.Drawing.Size(123, 20);
            this.CommitPassTB.TabIndex = 3;
            // 
            // NewPassTB
            // 
            this.NewPassTB.Location = new System.Drawing.Point(71, 19);
            this.NewPassTB.Name = "NewPassTB";
            this.NewPassTB.PasswordChar = '*';
            this.NewPassTB.Size = new System.Drawing.Size(123, 20);
            this.NewPassTB.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(225, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Conta criada em";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(225, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Última modificação";
            // 
            // CreationAccTB
            // 
            this.CreationAccTB.Location = new System.Drawing.Point(345, 13);
            this.CreationAccTB.Name = "CreationAccTB";
            this.CreationAccTB.ReadOnly = true;
            this.CreationAccTB.Size = new System.Drawing.Size(112, 20);
            this.CreationAccTB.TabIndex = 5;
            this.CreationAccTB.TabStop = false;
            // 
            // LastModifTB
            // 
            this.LastModifTB.Location = new System.Drawing.Point(345, 40);
            this.LastModifTB.Name = "LastModifTB";
            this.LastModifTB.ReadOnly = true;
            this.LastModifTB.Size = new System.Drawing.Size(112, 20);
            this.LastModifTB.TabIndex = 5;
            this.LastModifTB.TabStop = false;
            // 
            // SaveChangesBT
            // 
            this.SaveChangesBT.Location = new System.Drawing.Point(356, 239);
            this.SaveChangesBT.Name = "SaveChangesBT";
            this.SaveChangesBT.Size = new System.Drawing.Size(101, 23);
            this.SaveChangesBT.TabIndex = 5;
            this.SaveChangesBT.Text = "Salvar mudanças";
            this.SaveChangesBT.UseVisualStyleBackColor = true;
            this.SaveChangesBT.Click += new System.EventHandler(this.SaveChangesBT_Click);
            // 
            // UsuarioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 274);
            this.Controls.Add(this.SaveChangesBT);
            this.Controls.Add(this.LastModifTB);
            this.Controls.Add(this.CreationAccTB);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.MinuteLB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TimeTkB);
            this.Controls.Add(this.UserNameTB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UsuarioForm";
            this.Text = "Configurações de usuário";
            this.Load += new System.EventHandler(this.UsuarioForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TimeTkB)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UserNameTB;
        private System.Windows.Forms.TrackBar TimeTkB;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label MinuteLB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button CommitNewPassBt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox CommitPassTB;
        private System.Windows.Forms.TextBox NewPassTB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox CreationAccTB;
        private System.Windows.Forms.TextBox LastModifTB;
        private System.Windows.Forms.Button SaveChangesBT;
    }
}