﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using Valut.Accounts;
using Valut.Encrypting;
using Valut.FileManager;
using Valut.FileManager.Import;
using System.Security.Cryptography;

namespace Valut
{
    public delegate void SyncEventsCallback(object sender, EventArgs e);

    public partial class Form1 : Form
    {
        bool OnlyFav = false;
        private Conta SelectedAcc;

        public Form1()
        {
            InitializeComponent();
            InitializeView();
            AttachEvents();
        }

        private void NovaSenha_Click(object sender, EventArgs e)
        {
            RegistradorDeSenha RS = new RegistradorDeSenha();
            RS.ShowDialog();
        }

        void InitializeView()
        {
            VisualizadorContas.DataSource = UserCenterData.ExpressiveData;
            VisualizadorContas.Refresh();
        }

        void AttachEvents()
        {
            UserCenterData.SyncingFile += Desabilitarvisualizador;
            UserCenterData.FileSyncronized += Habilitarvisualizador;
        }

        private void ShowAdvDataBT_Click(object sender, EventArgs e)
        {
            IntegridadeLB.Visible = (IntegridadeLB.Visible) ? false : true;
            IntegridadeTB.Visible = (IntegridadeTB.Visible) ? false : true; ;
            ChecarIntegridadeBT.Visible = (ChecarIntegridadeBT.Visible) ? false : true; ;

            HashLB.Visible = (HashLB.Visible) ? false : true; ;
            HashTB.Visible = (HashTB.Visible) ? false : true; ;

            AssinaturaLB.Visible = (AssinaturaLB.Visible) ? false : true; ;
            AssinaturaTB.Visible = (AssinaturaTB.Visible) ? false : true; ;
        }

        private void VisualizadorContas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //FileStream mainFile = new FileStream(UserCenterData.MainFile, FileMode.Open);
            try
            {
                SelectedAcc = VisualizadorContas.CurrentRow.DataBoundItem as Conta; //Obtém o obj da celula selecionada
                var pass = PerfilThread.GetPK2(UserCenterData.GetPass(false)).PrivateKey();
                SelectedAcc.DecryptPassword(pass);

                DominioTB.Text = SelectedAcc.Dominio; //mostra na caixa de texto DominioTB o domínio da celula selecioanda
                UsuarioTB.Text = SelectedAcc.Usuario;
                CriadoEmTB.Text = SelectedAcc.CriadoEm.ToShortDateString();
                ModificadoEmTB.Text = SelectedAcc.ModificadoEm.ToShortDateString();
                PassTB.Text = SelectedAcc.Senha.ShowString();
                HashTB.Text = ConvertBytes(SelectedAcc.Hash);
                AssinaturaTB.Text = ConvertBytes(SelectedAcc.Assinatura);
                FavoritarBT.Text = SelectedAcc.Favorito ? "" : "";
                FavoritarBT.ForeColor = SelectedAcc.Favorito ? Color.Goldenrod : Control.DefaultForeColor;
                Default();
                SelectedAcc.EncryptPassword(PerfilThread.GetPK1(UserCenterData.GetPass(false)));
            }

            catch (CryptographicException)
            {
                MessageBox.Show("Chave incorreta, tente novamente!");
                UserCenterData.AbortPass();
            }

            catch (EncryptingExceptions.PasswordNotSetException) { }

            catch (EncryptingExceptions.IncorrrectPasswordException) { MessageBox.Show("Chave incorreta, tente novamente!"); }

            catch (ArgumentOutOfRangeException) { }

            catch (Exception ex)
            { MessageBox.Show($"Erro inesperado: {ex.Message}"); }
        }

        private void RevelarSenhaBt_MouseDown(object sender, MouseEventArgs e)
        {
            PassTB.PasswordChar = '\0';
        }

        private void RevelarSenhaBt_MouseUp(object sender, MouseEventArgs e)
        {
            PassTB.PasswordChar = '*';
        }

        public void RefreshData(object sender, EventArgs e)
        {
            
        }

        private string ConvertBytes(byte[] data)
        {
            string result = "";
            foreach(var dat in data)
            {
                result += dat + " ";
            }
            return result;
        }

        private bool VerifyIntegrity(Conta contaToVerify)
        {
            if (contaToVerify.Hash.Length == 0) return true;
            contaToVerify.DecryptPassword(PerfilThread.GetPK2(UserCenterData.GetPass(false)).PrivateKey());
            byte[] hash = contaToVerify.Hash;
            byte[] passw = Encoding.Unicode.GetBytes(contaToVerify.Senha.ShowString());

            contaToVerify.EncryptPassword(PerfilThread.GetPK1(UserCenterData.GetPass(false)));
            return SHA512Signer.VerifySignature(passw, hash);
        }

        private void ChecarIntegridadeBT_Click(object sender, EventArgs e)
        {
            if (SelectedAcc != null)
            {
                IntegridadeTB.BackColor = (VerifyIntegrity(SelectedAcc)) ? Color.LightSeaGreen : Color.Red;
            }
        }

        private void CopiarSenhaBT_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(PassTB.Text))
            {
                Clipboard.SetText(PassTB.Text);
                CopiarSenhaBT.BackColor = Color.ForestGreen;
                CopiarSenhaBT.ForeColor = Color.White;
                CopiarSenhaBT.Text = "";
            }
        }

        private void Default()
        {
            IntegridadeTB.BackColor = Control.DefaultBackColor;
            CopiarSenhaBT.BackColor = Control.DefaultBackColor;
            CopiarSenhaBT.Text = "";
            CopiarSenhaBT.ForeColor = Control.DefaultForeColor;
        }

        private void FavoritarBT_Click(object sender, EventArgs e)
        {
            if (SelectedAcc != null)
            {
                try
                {
                    UserCenterData.GetPass(true);
                    SelectedAcc.ModificadoEm = DateTime.Now;
                    SelectedAcc.Favorito = SelectedAcc.Favorito ? false : true;
                    FavoritarBT.Text = SelectedAcc.Favorito ? "" : "";
                    FavoritarBT.ForeColor = SelectedAcc.Favorito ? Color.Goldenrod : Control.DefaultForeColor;

                    UserCenterData.SyncData();
                }

                catch (EncryptingExceptions.IncorrrectPasswordException) { MessageBox.Show("Senha incorreta.");}
                catch(EncryptingExceptions.PasswordNotSetException) { }
            }
        }

        private void ExcluirContaBT_Click(object sender, EventArgs e)
        {
            if (SelectedAcc == null) return;
            if (MessageBox.Show("Tem certeza que deseja remover esta conta?", "Remover Senha", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                try
                {
                    UserCenterData.GetPass(true);
                    ignoreCall = true;
                    UserCenterData.ExpressiveData.Remove(SelectedAcc);
                    SelectedAcc = null;
                    Limpar();

                    UserCenterData.SyncData();
                }

                catch(EncryptingExceptions.IncorrrectPasswordException)
                {
                    MessageBox.Show("Senha incorreta");
                }
                catch (EncryptingExceptions.PasswordNotSetException) { }
            }           
        }

        private void SobreBT_Click(object sender, EventArgs e)
        {
            AboutBox1 abb = new AboutBox1();
            abb.ShowDialog();
        }

        private void ConfigSenhaBT_Click(object sender, EventArgs e)
        {
            if (SelectedAcc != null)
            {
                try
                {

                    RegistradorDeSenha rds = new RegistradorDeSenha(SelectedAcc);
                    rds.ShowDialog();
                    VisualizadorContas_CellClick(this, null);
                }
                catch (EncryptingExceptions.IncorrrectPasswordException) { MessageBox.Show("Senha Incorreta");}
                catch (Exception) { }
            }
        }

        private void FiltarFavoritosBT_Click(object sender, EventArgs e)
        {
            FiltarFavoritosBT.Text = OnlyFav ? "" : "";
            OnlyFav = OnlyFav ? false : true;
        }

        private void FiltragemTB_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(FiltragemTB.Text))
            {
                var queryD = from q in UserCenterData.ExpressiveData
                             where q.Dominio.ToLower().Contains(FiltragemTB.Text.ToLower())
                             select q;

                var queryU = from q in UserCenterData.ExpressiveData
                             where q.Usuario.ToLower().Contains(FiltragemTB.Text.ToLower())
                             select q;

                var resultOP = (DominioRBT.Checked) ? queryD : queryU;

                BindingList<Conta> filter = new BindingList<Conta>();
                foreach (var obj in resultOP)
                {
                    if (OnlyFav && obj.Favorito)
                    { filter.Add(obj); }
                    else if (!OnlyFav) filter.Add(obj);
                }

                VisualizadorContas.DataSource = filter;
            }
            else
            {
                VisualizadorContas.DataSource = UserCenterData.ExpressiveData;
            }
        }

        private void Desabilitarvisualizador(object sender, EventArgs e)
        {
            if (VisualizadorContas.InvokeRequired)
            {
                SyncEventsCallback s = new SyncEventsCallback(Desabilitarvisualizador);
                this.Invoke(s,new object[] { null, null });
            }
            else VisualizadorContas.Enabled = false;
        }

        private void Habilitarvisualizador(object sender, EventArgs e)
        {
            if (VisualizadorContas.InvokeRequired)
            {
                SyncEventsCallback s = new SyncEventsCallback(Habilitarvisualizador);
                this.Invoke(s, new object[] { null, null });
            }
            else VisualizadorContas.Enabled = true;
        }

        private void LaunchImportingWizardBT_Click(object sender, EventArgs e)
        {
            ImportingWizardForm iwf = new ImportingWizardForm();
            iwf.ShowDialog();
        }

        private void UsuarioBT_Click(object sender, EventArgs e)
        {
            UsuarioForm userform = new UsuarioForm();
            userform.ShowDialog();
        }

        bool changed = false, ignoreCall = true;
        private void VisualizadorContas_SelectionChanged(object sender, EventArgs e)
        {
            changed = true;
            if (ignoreCall) { ignoreCall = false; }
            else { VisualizadorContas_CellClick(null, null); }
        }

        private void VisualizadorContas_MouseUp(object sender, MouseEventArgs e)
        {
            if (changed)
            {
                changed = false;
            }
            else
            {
                Limpar();
                changed = true;
                SelectedAcc = null;
            }
        }

        private void Limpar()
        {
            DominioTB.Text = "";
            UsuarioTB.Text = "";
            CriadoEmTB.Text = "";
            ModificadoEmTB.Text = "";
            PassTB.Text = "";
            HashTB.Text = "";
            AssinaturaTB.Text = "";
            FavoritarBT.Text = "";
            Default();
        }
    }
}
    