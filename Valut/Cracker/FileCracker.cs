﻿using System;
using System.IO;

namespace Valut.Cracker
{
    static internal class FileCracker
    {
        static public void CorruptFile(FileStream fs)
        {
            MemoryStream ms = new MemoryStream();
            fs.CopyTo(ms);

            fs.Close();
            byte[] buff = ms.GetBuffer();

            Random r = new Random();
            int indexes = r.Next(0, 30);

            for(int i=0;i<indexes;i++)
            {
                buff[r.Next(0, buff.Length)] = (byte)r.Next(0, 256);
            }

            File.SetAttributes(fs.Name, FileAttributes.Normal);
            fs = new FileStream(fs.Name, FileMode.Create);
            fs.Write(buff, 0, buff.Length);
            fs.Close();
        }
    }
}
