﻿using System;
using System.Windows.Forms;
using System.Security;
using Valut.Encrypting;

namespace Valut
{
    public partial class UserGather : Form
    {
        public UserGather()
        {
            InitializeComponent();
        }

        public string GatherPerfilData(out SecureString password)
        {
            if(this.ShowDialog() == DialogResult.OK)
            {
                password = SenhaTB.Text.HideString().Copy();

                return UsuarioTB.Text;
            }

            else
            {
                throw new Exception("Não é possível continuar o processo sem os dados confirmados!");
            }
        }

        private void Commit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(UsuarioTB.Text))
            {
                if (string.Equals(SenhaTB.Text, RSenhaTB.Text) && !string.IsNullOrWhiteSpace(SenhaTB.Text))
                {
                    if (AgreementCB.Checked) this.DialogResult = DialogResult.OK;
                    else MessageBox.Show("Para continuar é preciso confirmar a ciência das instruções de segurança");
                }

                else
                {
                    MessageBox.Show("As senhas não conferem ou estão em branco, tente novamente!");
                    SenhaTB.Clear();
                    RSenhaTB.Clear();
                }
            }

            else
            {
                MessageBox.Show("Nome de usuário inválido!");
                UsuarioTB.Clear();
            }
        }

        private void UserGather_Load(object sender, EventArgs e)
        {
            richTextBox1.Text = Valut.Properties.Resources.User_Instruction;
        }
    }
}
